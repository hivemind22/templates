// Compiled by ClojureScript 0.0-2913 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
cljs.core.async.fn_handler = (function fn_handler(f){
if(typeof cljs.core.async.t31091 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t31091 = (function (f,fn_handler,meta31092){
this.f = f;
this.fn_handler = fn_handler;
this.meta31092 = meta31092;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t31091.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t31091.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t31091.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t31091.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31093){
var self__ = this;
var _31093__$1 = this;
return self__.meta31092;
});

cljs.core.async.t31091.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31093,meta31092__$1){
var self__ = this;
var _31093__$1 = this;
return (new cljs.core.async.t31091(self__.f,self__.fn_handler,meta31092__$1));
});

cljs.core.async.t31091.cljs$lang$type = true;

cljs.core.async.t31091.cljs$lang$ctorStr = "cljs.core.async/t31091";

cljs.core.async.t31091.cljs$lang$ctorPrWriter = (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t31091");
});

cljs.core.async.__GT_t31091 = (function __GT_t31091(f__$1,fn_handler__$1,meta31092){
return (new cljs.core.async.t31091(f__$1,fn_handler__$1,meta31092));
});

}

return (new cljs.core.async.t31091(f,fn_handler,cljs.core.PersistentArrayMap.EMPTY));
});
/**
* Returns a fixed buffer of size n. When full, puts will block/park.
*/
cljs.core.async.buffer = (function buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete but
* val will be dropped (no transfer).
*/
cljs.core.async.dropping_buffer = (function dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete, and be
* buffered, but oldest elements in buffer will be dropped (not
* transferred).
*/
cljs.core.async.sliding_buffer = (function sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
* Returns true if a channel created with buff will never block. That is to say,
* puts into this buffer will never cause the buffer to be full.
*/
cljs.core.async.unblocking_buffer_QMARK_ = (function unblocking_buffer_QMARK_(buff){
var G__31095 = buff;
if(G__31095){
var bit__4662__auto__ = null;
if(cljs.core.truth_((function (){var or__3981__auto__ = bit__4662__auto__;
if(cljs.core.truth_(or__3981__auto__)){
return or__3981__auto__;
} else {
return G__31095.cljs$core$async$impl$protocols$UnblockingBuffer$;
}
})())){
return true;
} else {
if((!G__31095.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__31095);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__31095);
}
});
/**
* Creates a channel with an optional buffer, an optional transducer (like (map f),
* (filter p) etc or a composition thereof), and an optional exception handler.
* If buf-or-n is a number, will create and use a fixed buffer of that size. If a
* transducer is supplied a buffer must be specified. ex-handler must be a
* fn of one argument - if an exception occurs during transformation it will be called
* with the thrown value as an argument, and any non-nil return value will be placed
* in the channel.
*/
cljs.core.async.chan = (function() {
var chan = null;
var chan__0 = (function (){
return chan.call(null,null);
});
var chan__1 = (function (buf_or_n){
return chan.call(null,buf_or_n,null,null);
});
var chan__2 = (function (buf_or_n,xform){
return chan.call(null,buf_or_n,xform,null);
});
var chan__3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("buffer must be supplied when transducer is"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"buf-or-n","buf-or-n",-1646815050,null)))].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});
chan = function(buf_or_n,xform,ex_handler){
switch(arguments.length){
case 0:
return chan__0.call(this);
case 1:
return chan__1.call(this,buf_or_n);
case 2:
return chan__2.call(this,buf_or_n,xform);
case 3:
return chan__3.call(this,buf_or_n,xform,ex_handler);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
chan.cljs$core$IFn$_invoke$arity$0 = chan__0;
chan.cljs$core$IFn$_invoke$arity$1 = chan__1;
chan.cljs$core$IFn$_invoke$arity$2 = chan__2;
chan.cljs$core$IFn$_invoke$arity$3 = chan__3;
return chan;
})()
;
/**
* Returns a channel that will close after msecs
*/
cljs.core.async.timeout = (function timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
* takes a val from port. Must be called inside a (go ...) block. Will
* return nil if closed. Will park if nothing is available.
* Returns true unless port is already closed
*/
cljs.core.async._LT__BANG_ = (function _LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
* Asynchronously takes a val from port, passing to fn1. Will pass nil
* if closed. If on-caller? (default true) is true, and value is
* immediately available, will call fn1 on calling thread.
* Returns nil.
*/
cljs.core.async.take_BANG_ = (function() {
var take_BANG_ = null;
var take_BANG___2 = (function (port,fn1){
return take_BANG_.call(null,port,fn1,true);
});
var take_BANG___3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_31096 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_31096);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_31096,ret){
return (function (){
return fn1.call(null,val_31096);
});})(val_31096,ret))
);
}
} else {
}

return null;
});
take_BANG_ = function(port,fn1,on_caller_QMARK_){
switch(arguments.length){
case 2:
return take_BANG___2.call(this,port,fn1);
case 3:
return take_BANG___3.call(this,port,fn1,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take_BANG_.cljs$core$IFn$_invoke$arity$2 = take_BANG___2;
take_BANG_.cljs$core$IFn$_invoke$arity$3 = take_BANG___3;
return take_BANG_;
})()
;
cljs.core.async.nop = (function nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
* puts a val into port. nil values are not allowed. Must be called
* inside a (go ...) block. Will park if no buffer space is available.
* Returns true unless port is already closed.
*/
cljs.core.async._GT__BANG_ = (function _GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
* Asynchronously puts a val into port, calling fn0 (if supplied) when
* complete. nil values are not allowed. Will throw if closed. If
* on-caller? (default true) is true, and the put is immediately
* accepted, will call fn0 on calling thread.  Returns nil.
*/
cljs.core.async.put_BANG_ = (function() {
var put_BANG_ = null;
var put_BANG___2 = (function (port,val){
var temp__4124__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4124__auto__)){
var ret = temp__4124__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});
var put_BANG___3 = (function (port,val,fn1){
return put_BANG_.call(null,port,val,fn1,true);
});
var put_BANG___4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4124__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4124__auto__)){
var retb = temp__4124__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4124__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4124__auto__))
);
}

return ret;
} else {
return true;
}
});
put_BANG_ = function(port,val,fn1,on_caller_QMARK_){
switch(arguments.length){
case 2:
return put_BANG___2.call(this,port,val);
case 3:
return put_BANG___3.call(this,port,val,fn1);
case 4:
return put_BANG___4.call(this,port,val,fn1,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
put_BANG_.cljs$core$IFn$_invoke$arity$2 = put_BANG___2;
put_BANG_.cljs$core$IFn$_invoke$arity$3 = put_BANG___3;
put_BANG_.cljs$core$IFn$_invoke$arity$4 = put_BANG___4;
return put_BANG_;
})()
;
cljs.core.async.close_BANG_ = (function close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function random_array(n){
var a = (new Array(n));
var n__4868__auto___31097 = n;
var x_31098 = (0);
while(true){
if((x_31098 < n__4868__auto___31097)){
(a[x_31098] = (0));

var G__31099 = (x_31098 + (1));
x_31098 = G__31099;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__31100 = (i + (1));
i = G__31100;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t31104 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t31104 = (function (flag,alt_flag,meta31105){
this.flag = flag;
this.alt_flag = alt_flag;
this.meta31105 = meta31105;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t31104.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t31104.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t31104.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t31104.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_31106){
var self__ = this;
var _31106__$1 = this;
return self__.meta31105;
});})(flag))
;

cljs.core.async.t31104.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_31106,meta31105__$1){
var self__ = this;
var _31106__$1 = this;
return (new cljs.core.async.t31104(self__.flag,self__.alt_flag,meta31105__$1));
});})(flag))
;

cljs.core.async.t31104.cljs$lang$type = true;

cljs.core.async.t31104.cljs$lang$ctorStr = "cljs.core.async/t31104";

cljs.core.async.t31104.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t31104");
});})(flag))
;

cljs.core.async.__GT_t31104 = ((function (flag){
return (function __GT_t31104(flag__$1,alt_flag__$1,meta31105){
return (new cljs.core.async.t31104(flag__$1,alt_flag__$1,meta31105));
});})(flag))
;

}

return (new cljs.core.async.t31104(flag,alt_flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function alt_handler(flag,cb){
if(typeof cljs.core.async.t31110 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t31110 = (function (cb,flag,alt_handler,meta31111){
this.cb = cb;
this.flag = flag;
this.alt_handler = alt_handler;
this.meta31111 = meta31111;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t31110.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t31110.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t31110.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t31110.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31112){
var self__ = this;
var _31112__$1 = this;
return self__.meta31111;
});

cljs.core.async.t31110.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31112,meta31111__$1){
var self__ = this;
var _31112__$1 = this;
return (new cljs.core.async.t31110(self__.cb,self__.flag,self__.alt_handler,meta31111__$1));
});

cljs.core.async.t31110.cljs$lang$type = true;

cljs.core.async.t31110.cljs$lang$ctorStr = "cljs.core.async/t31110";

cljs.core.async.t31110.cljs$lang$ctorPrWriter = (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t31110");
});

cljs.core.async.__GT_t31110 = (function __GT_t31110(cb__$1,flag__$1,alt_handler__$1,meta31111){
return (new cljs.core.async.t31110(cb__$1,flag__$1,alt_handler__$1,meta31111));
});

}

return (new cljs.core.async.t31110(cb,flag,alt_handler,cljs.core.PersistentArrayMap.EMPTY));
});
/**
* returns derefable [val port] if immediate, nil if enqueued
*/
cljs.core.async.do_alts = (function do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__31113_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__31113_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__31114_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__31114_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__3981__auto__ = wport;
if(cljs.core.truth_(or__3981__auto__)){
return or__3981__auto__;
} else {
return port;
}
})()], null));
} else {
var G__31115 = (i + (1));
i = G__31115;
continue;
}
} else {
return null;
}
break;
}
})();
var or__3981__auto__ = ret;
if(cljs.core.truth_(or__3981__auto__)){
return or__3981__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4126__auto__ = (function (){var and__3969__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__3969__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__3969__auto__;
}
})();
if(cljs.core.truth_(temp__4126__auto__)){
var got = temp__4126__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
* Completes at most one of several channel operations. Must be called
* inside a (go ...) block. ports is a vector of channel endpoints,
* which can be either a channel to take from or a vector of
* [channel-to-put-to val-to-put], in any combination. Takes will be
* made as if by <!, and puts will be made as if by >!. Unless
* the :priority option is true, if more than one port operation is
* ready a non-deterministic choice will be made. If no operation is
* ready and a :default value is supplied, [default-val :default] will
* be returned, otherwise alts! will park until the first operation to
* become ready completes. Returns [val port] of the completed
* operation, where val is the value taken for takes, and a
* boolean (true unless already closed, as per put!) for puts.
* 
* opts are passed as :key val ... Supported options:
* 
* :default val - the value to use if none of the operations are immediately ready
* :priority true - (default nil) when true, the operations will be tried in order.
* 
* Note: there is no guarantee that the port exps or val exprs will be
* used, nor in what order should they be, so they should not be
* depended upon for side effects.
* @param {...*} var_args
*/
cljs.core.async.alts_BANG_ = (function() { 
var alts_BANG___delegate = function (ports,p__31116){
var map__31118 = p__31116;
var map__31118__$1 = ((cljs.core.seq_QMARK_.call(null,map__31118))?cljs.core.apply.call(null,cljs.core.hash_map,map__31118):map__31118);
var opts = map__31118__$1;
throw (new Error("alts! used not in (go ...) block"));
};
var alts_BANG_ = function (ports,var_args){
var p__31116 = null;
if (arguments.length > 1) {
var G__31119__i = 0, G__31119__a = new Array(arguments.length -  1);
while (G__31119__i < G__31119__a.length) {G__31119__a[G__31119__i] = arguments[G__31119__i + 1]; ++G__31119__i;}
  p__31116 = new cljs.core.IndexedSeq(G__31119__a,0);
} 
return alts_BANG___delegate.call(this,ports,p__31116);};
alts_BANG_.cljs$lang$maxFixedArity = 1;
alts_BANG_.cljs$lang$applyTo = (function (arglist__31120){
var ports = cljs.core.first(arglist__31120);
var p__31116 = cljs.core.rest(arglist__31120);
return alts_BANG___delegate(ports,p__31116);
});
alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = alts_BANG___delegate;
return alts_BANG_;
})()
;
/**
* Takes elements from the from channel and supplies them to the to
* channel. By default, the to channel will be closed when the from
* channel closes, but can be determined by the close?  parameter. Will
* stop consuming the from channel if the to channel closes
*/
cljs.core.async.pipe = (function() {
var pipe = null;
var pipe__2 = (function (from,to){
return pipe.call(null,from,to,true);
});
var pipe__3 = (function (from,to,close_QMARK_){
var c__7600__auto___31215 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___31215){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___31215){
return (function (state_31191){
var state_val_31192 = (state_31191[(1)]);
if((state_val_31192 === (7))){
var inst_31187 = (state_31191[(2)]);
var state_31191__$1 = state_31191;
var statearr_31193_31216 = state_31191__$1;
(statearr_31193_31216[(2)] = inst_31187);

(statearr_31193_31216[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (1))){
var state_31191__$1 = state_31191;
var statearr_31194_31217 = state_31191__$1;
(statearr_31194_31217[(2)] = null);

(statearr_31194_31217[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (4))){
var inst_31170 = (state_31191[(7)]);
var inst_31170__$1 = (state_31191[(2)]);
var inst_31171 = (inst_31170__$1 == null);
var state_31191__$1 = (function (){var statearr_31195 = state_31191;
(statearr_31195[(7)] = inst_31170__$1);

return statearr_31195;
})();
if(cljs.core.truth_(inst_31171)){
var statearr_31196_31218 = state_31191__$1;
(statearr_31196_31218[(1)] = (5));

} else {
var statearr_31197_31219 = state_31191__$1;
(statearr_31197_31219[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (13))){
var state_31191__$1 = state_31191;
var statearr_31198_31220 = state_31191__$1;
(statearr_31198_31220[(2)] = null);

(statearr_31198_31220[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (6))){
var inst_31170 = (state_31191[(7)]);
var state_31191__$1 = state_31191;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31191__$1,(11),to,inst_31170);
} else {
if((state_val_31192 === (3))){
var inst_31189 = (state_31191[(2)]);
var state_31191__$1 = state_31191;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31191__$1,inst_31189);
} else {
if((state_val_31192 === (12))){
var state_31191__$1 = state_31191;
var statearr_31199_31221 = state_31191__$1;
(statearr_31199_31221[(2)] = null);

(statearr_31199_31221[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (2))){
var state_31191__$1 = state_31191;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31191__$1,(4),from);
} else {
if((state_val_31192 === (11))){
var inst_31180 = (state_31191[(2)]);
var state_31191__$1 = state_31191;
if(cljs.core.truth_(inst_31180)){
var statearr_31200_31222 = state_31191__$1;
(statearr_31200_31222[(1)] = (12));

} else {
var statearr_31201_31223 = state_31191__$1;
(statearr_31201_31223[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (9))){
var state_31191__$1 = state_31191;
var statearr_31202_31224 = state_31191__$1;
(statearr_31202_31224[(2)] = null);

(statearr_31202_31224[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (5))){
var state_31191__$1 = state_31191;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31203_31225 = state_31191__$1;
(statearr_31203_31225[(1)] = (8));

} else {
var statearr_31204_31226 = state_31191__$1;
(statearr_31204_31226[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (14))){
var inst_31185 = (state_31191[(2)]);
var state_31191__$1 = state_31191;
var statearr_31205_31227 = state_31191__$1;
(statearr_31205_31227[(2)] = inst_31185);

(statearr_31205_31227[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (10))){
var inst_31177 = (state_31191[(2)]);
var state_31191__$1 = state_31191;
var statearr_31206_31228 = state_31191__$1;
(statearr_31206_31228[(2)] = inst_31177);

(statearr_31206_31228[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31192 === (8))){
var inst_31174 = cljs.core.async.close_BANG_.call(null,to);
var state_31191__$1 = state_31191;
var statearr_31207_31229 = state_31191__$1;
(statearr_31207_31229[(2)] = inst_31174);

(statearr_31207_31229[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___31215))
;
return ((function (switch__7585__auto__,c__7600__auto___31215){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31211 = [null,null,null,null,null,null,null,null];
(statearr_31211[(0)] = state_machine__7586__auto__);

(statearr_31211[(1)] = (1));

return statearr_31211;
});
var state_machine__7586__auto____1 = (function (state_31191){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31191);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31212){if((e31212 instanceof Object)){
var ex__7589__auto__ = e31212;
var statearr_31213_31230 = state_31191;
(statearr_31213_31230[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31191);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31212;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31231 = state_31191;
state_31191 = G__31231;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31191){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31191);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___31215))
})();
var state__7602__auto__ = (function (){var statearr_31214 = f__7601__auto__.call(null);
(statearr_31214[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___31215);

return statearr_31214;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___31215))
);


return to;
});
pipe = function(from,to,close_QMARK_){
switch(arguments.length){
case 2:
return pipe__2.call(this,from,to);
case 3:
return pipe__3.call(this,from,to,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pipe.cljs$core$IFn$_invoke$arity$2 = pipe__2;
pipe.cljs$core$IFn$_invoke$arity$3 = pipe__3;
return pipe;
})()
;
cljs.core.async.pipeline_STAR_ = (function pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"pos?","pos?",-244377722,null),new cljs.core.Symbol(null,"n","n",-2092305744,null))))].join('')));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__31415){
var vec__31416 = p__31415;
var v = cljs.core.nth.call(null,vec__31416,(0),null);
var p = cljs.core.nth.call(null,vec__31416,(1),null);
var job = vec__31416;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__7600__auto___31598 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___31598,res,vec__31416,v,p,job,jobs,results){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___31598,res,vec__31416,v,p,job,jobs,results){
return (function (state_31421){
var state_val_31422 = (state_31421[(1)]);
if((state_val_31422 === (2))){
var inst_31418 = (state_31421[(2)]);
var inst_31419 = cljs.core.async.close_BANG_.call(null,res);
var state_31421__$1 = (function (){var statearr_31423 = state_31421;
(statearr_31423[(7)] = inst_31418);

return statearr_31423;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31421__$1,inst_31419);
} else {
if((state_val_31422 === (1))){
var state_31421__$1 = state_31421;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31421__$1,(2),res,v);
} else {
return null;
}
}
});})(c__7600__auto___31598,res,vec__31416,v,p,job,jobs,results))
;
return ((function (switch__7585__auto__,c__7600__auto___31598,res,vec__31416,v,p,job,jobs,results){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31427 = [null,null,null,null,null,null,null,null];
(statearr_31427[(0)] = state_machine__7586__auto__);

(statearr_31427[(1)] = (1));

return statearr_31427;
});
var state_machine__7586__auto____1 = (function (state_31421){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31421);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31428){if((e31428 instanceof Object)){
var ex__7589__auto__ = e31428;
var statearr_31429_31599 = state_31421;
(statearr_31429_31599[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31421);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31428;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31600 = state_31421;
state_31421 = G__31600;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31421){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31421);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___31598,res,vec__31416,v,p,job,jobs,results))
})();
var state__7602__auto__ = (function (){var statearr_31430 = f__7601__auto__.call(null);
(statearr_31430[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___31598);

return statearr_31430;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___31598,res,vec__31416,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__31431){
var vec__31432 = p__31431;
var v = cljs.core.nth.call(null,vec__31432,(0),null);
var p = cljs.core.nth.call(null,vec__31432,(1),null);
var job = vec__31432;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__4868__auto___31601 = n;
var __31602 = (0);
while(true){
if((__31602 < n__4868__auto___31601)){
var G__31433_31603 = (((type instanceof cljs.core.Keyword))?type.fqn:null);
switch (G__31433_31603) {
case "async":
var c__7600__auto___31605 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__31602,c__7600__auto___31605,G__31433_31603,n__4868__auto___31601,jobs,results,process,async){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (__31602,c__7600__auto___31605,G__31433_31603,n__4868__auto___31601,jobs,results,process,async){
return (function (state_31446){
var state_val_31447 = (state_31446[(1)]);
if((state_val_31447 === (7))){
var inst_31442 = (state_31446[(2)]);
var state_31446__$1 = state_31446;
var statearr_31448_31606 = state_31446__$1;
(statearr_31448_31606[(2)] = inst_31442);

(statearr_31448_31606[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31447 === (6))){
var state_31446__$1 = state_31446;
var statearr_31449_31607 = state_31446__$1;
(statearr_31449_31607[(2)] = null);

(statearr_31449_31607[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31447 === (5))){
var state_31446__$1 = state_31446;
var statearr_31450_31608 = state_31446__$1;
(statearr_31450_31608[(2)] = null);

(statearr_31450_31608[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31447 === (4))){
var inst_31436 = (state_31446[(2)]);
var inst_31437 = async.call(null,inst_31436);
var state_31446__$1 = state_31446;
if(cljs.core.truth_(inst_31437)){
var statearr_31451_31609 = state_31446__$1;
(statearr_31451_31609[(1)] = (5));

} else {
var statearr_31452_31610 = state_31446__$1;
(statearr_31452_31610[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31447 === (3))){
var inst_31444 = (state_31446[(2)]);
var state_31446__$1 = state_31446;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31446__$1,inst_31444);
} else {
if((state_val_31447 === (2))){
var state_31446__$1 = state_31446;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31446__$1,(4),jobs);
} else {
if((state_val_31447 === (1))){
var state_31446__$1 = state_31446;
var statearr_31453_31611 = state_31446__$1;
(statearr_31453_31611[(2)] = null);

(statearr_31453_31611[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__31602,c__7600__auto___31605,G__31433_31603,n__4868__auto___31601,jobs,results,process,async))
;
return ((function (__31602,switch__7585__auto__,c__7600__auto___31605,G__31433_31603,n__4868__auto___31601,jobs,results,process,async){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31457 = [null,null,null,null,null,null,null];
(statearr_31457[(0)] = state_machine__7586__auto__);

(statearr_31457[(1)] = (1));

return statearr_31457;
});
var state_machine__7586__auto____1 = (function (state_31446){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31446);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31458){if((e31458 instanceof Object)){
var ex__7589__auto__ = e31458;
var statearr_31459_31612 = state_31446;
(statearr_31459_31612[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31446);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31458;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31613 = state_31446;
state_31446 = G__31613;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31446){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31446);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(__31602,switch__7585__auto__,c__7600__auto___31605,G__31433_31603,n__4868__auto___31601,jobs,results,process,async))
})();
var state__7602__auto__ = (function (){var statearr_31460 = f__7601__auto__.call(null);
(statearr_31460[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___31605);

return statearr_31460;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(__31602,c__7600__auto___31605,G__31433_31603,n__4868__auto___31601,jobs,results,process,async))
);


break;
case "compute":
var c__7600__auto___31614 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__31602,c__7600__auto___31614,G__31433_31603,n__4868__auto___31601,jobs,results,process,async){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (__31602,c__7600__auto___31614,G__31433_31603,n__4868__auto___31601,jobs,results,process,async){
return (function (state_31473){
var state_val_31474 = (state_31473[(1)]);
if((state_val_31474 === (7))){
var inst_31469 = (state_31473[(2)]);
var state_31473__$1 = state_31473;
var statearr_31475_31615 = state_31473__$1;
(statearr_31475_31615[(2)] = inst_31469);

(statearr_31475_31615[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31474 === (6))){
var state_31473__$1 = state_31473;
var statearr_31476_31616 = state_31473__$1;
(statearr_31476_31616[(2)] = null);

(statearr_31476_31616[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31474 === (5))){
var state_31473__$1 = state_31473;
var statearr_31477_31617 = state_31473__$1;
(statearr_31477_31617[(2)] = null);

(statearr_31477_31617[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31474 === (4))){
var inst_31463 = (state_31473[(2)]);
var inst_31464 = process.call(null,inst_31463);
var state_31473__$1 = state_31473;
if(cljs.core.truth_(inst_31464)){
var statearr_31478_31618 = state_31473__$1;
(statearr_31478_31618[(1)] = (5));

} else {
var statearr_31479_31619 = state_31473__$1;
(statearr_31479_31619[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31474 === (3))){
var inst_31471 = (state_31473[(2)]);
var state_31473__$1 = state_31473;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31473__$1,inst_31471);
} else {
if((state_val_31474 === (2))){
var state_31473__$1 = state_31473;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31473__$1,(4),jobs);
} else {
if((state_val_31474 === (1))){
var state_31473__$1 = state_31473;
var statearr_31480_31620 = state_31473__$1;
(statearr_31480_31620[(2)] = null);

(statearr_31480_31620[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__31602,c__7600__auto___31614,G__31433_31603,n__4868__auto___31601,jobs,results,process,async))
;
return ((function (__31602,switch__7585__auto__,c__7600__auto___31614,G__31433_31603,n__4868__auto___31601,jobs,results,process,async){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31484 = [null,null,null,null,null,null,null];
(statearr_31484[(0)] = state_machine__7586__auto__);

(statearr_31484[(1)] = (1));

return statearr_31484;
});
var state_machine__7586__auto____1 = (function (state_31473){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31473);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31485){if((e31485 instanceof Object)){
var ex__7589__auto__ = e31485;
var statearr_31486_31621 = state_31473;
(statearr_31486_31621[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31473);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31485;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31622 = state_31473;
state_31473 = G__31622;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31473){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31473);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(__31602,switch__7585__auto__,c__7600__auto___31614,G__31433_31603,n__4868__auto___31601,jobs,results,process,async))
})();
var state__7602__auto__ = (function (){var statearr_31487 = f__7601__auto__.call(null);
(statearr_31487[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___31614);

return statearr_31487;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(__31602,c__7600__auto___31614,G__31433_31603,n__4868__auto___31601,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(type)].join('')));

}

var G__31623 = (__31602 + (1));
__31602 = G__31623;
continue;
} else {
}
break;
}

var c__7600__auto___31624 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___31624,jobs,results,process,async){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___31624,jobs,results,process,async){
return (function (state_31509){
var state_val_31510 = (state_31509[(1)]);
if((state_val_31510 === (9))){
var inst_31502 = (state_31509[(2)]);
var state_31509__$1 = (function (){var statearr_31511 = state_31509;
(statearr_31511[(7)] = inst_31502);

return statearr_31511;
})();
var statearr_31512_31625 = state_31509__$1;
(statearr_31512_31625[(2)] = null);

(statearr_31512_31625[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31510 === (8))){
var inst_31495 = (state_31509[(8)]);
var inst_31500 = (state_31509[(2)]);
var state_31509__$1 = (function (){var statearr_31513 = state_31509;
(statearr_31513[(9)] = inst_31500);

return statearr_31513;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31509__$1,(9),results,inst_31495);
} else {
if((state_val_31510 === (7))){
var inst_31505 = (state_31509[(2)]);
var state_31509__$1 = state_31509;
var statearr_31514_31626 = state_31509__$1;
(statearr_31514_31626[(2)] = inst_31505);

(statearr_31514_31626[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31510 === (6))){
var inst_31495 = (state_31509[(8)]);
var inst_31490 = (state_31509[(10)]);
var inst_31495__$1 = cljs.core.async.chan.call(null,(1));
var inst_31496 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_31497 = [inst_31490,inst_31495__$1];
var inst_31498 = (new cljs.core.PersistentVector(null,2,(5),inst_31496,inst_31497,null));
var state_31509__$1 = (function (){var statearr_31515 = state_31509;
(statearr_31515[(8)] = inst_31495__$1);

return statearr_31515;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31509__$1,(8),jobs,inst_31498);
} else {
if((state_val_31510 === (5))){
var inst_31493 = cljs.core.async.close_BANG_.call(null,jobs);
var state_31509__$1 = state_31509;
var statearr_31516_31627 = state_31509__$1;
(statearr_31516_31627[(2)] = inst_31493);

(statearr_31516_31627[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31510 === (4))){
var inst_31490 = (state_31509[(10)]);
var inst_31490__$1 = (state_31509[(2)]);
var inst_31491 = (inst_31490__$1 == null);
var state_31509__$1 = (function (){var statearr_31517 = state_31509;
(statearr_31517[(10)] = inst_31490__$1);

return statearr_31517;
})();
if(cljs.core.truth_(inst_31491)){
var statearr_31518_31628 = state_31509__$1;
(statearr_31518_31628[(1)] = (5));

} else {
var statearr_31519_31629 = state_31509__$1;
(statearr_31519_31629[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31510 === (3))){
var inst_31507 = (state_31509[(2)]);
var state_31509__$1 = state_31509;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31509__$1,inst_31507);
} else {
if((state_val_31510 === (2))){
var state_31509__$1 = state_31509;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31509__$1,(4),from);
} else {
if((state_val_31510 === (1))){
var state_31509__$1 = state_31509;
var statearr_31520_31630 = state_31509__$1;
(statearr_31520_31630[(2)] = null);

(statearr_31520_31630[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___31624,jobs,results,process,async))
;
return ((function (switch__7585__auto__,c__7600__auto___31624,jobs,results,process,async){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31524 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31524[(0)] = state_machine__7586__auto__);

(statearr_31524[(1)] = (1));

return statearr_31524;
});
var state_machine__7586__auto____1 = (function (state_31509){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31509);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31525){if((e31525 instanceof Object)){
var ex__7589__auto__ = e31525;
var statearr_31526_31631 = state_31509;
(statearr_31526_31631[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31509);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31525;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31632 = state_31509;
state_31509 = G__31632;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31509){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31509);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___31624,jobs,results,process,async))
})();
var state__7602__auto__ = (function (){var statearr_31527 = f__7601__auto__.call(null);
(statearr_31527[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___31624);

return statearr_31527;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___31624,jobs,results,process,async))
);


var c__7600__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto__,jobs,results,process,async){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto__,jobs,results,process,async){
return (function (state_31565){
var state_val_31566 = (state_31565[(1)]);
if((state_val_31566 === (7))){
var inst_31561 = (state_31565[(2)]);
var state_31565__$1 = state_31565;
var statearr_31567_31633 = state_31565__$1;
(statearr_31567_31633[(2)] = inst_31561);

(statearr_31567_31633[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (20))){
var state_31565__$1 = state_31565;
var statearr_31568_31634 = state_31565__$1;
(statearr_31568_31634[(2)] = null);

(statearr_31568_31634[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (1))){
var state_31565__$1 = state_31565;
var statearr_31569_31635 = state_31565__$1;
(statearr_31569_31635[(2)] = null);

(statearr_31569_31635[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (4))){
var inst_31530 = (state_31565[(7)]);
var inst_31530__$1 = (state_31565[(2)]);
var inst_31531 = (inst_31530__$1 == null);
var state_31565__$1 = (function (){var statearr_31570 = state_31565;
(statearr_31570[(7)] = inst_31530__$1);

return statearr_31570;
})();
if(cljs.core.truth_(inst_31531)){
var statearr_31571_31636 = state_31565__$1;
(statearr_31571_31636[(1)] = (5));

} else {
var statearr_31572_31637 = state_31565__$1;
(statearr_31572_31637[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (15))){
var inst_31543 = (state_31565[(8)]);
var state_31565__$1 = state_31565;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31565__$1,(18),to,inst_31543);
} else {
if((state_val_31566 === (21))){
var inst_31556 = (state_31565[(2)]);
var state_31565__$1 = state_31565;
var statearr_31573_31638 = state_31565__$1;
(statearr_31573_31638[(2)] = inst_31556);

(statearr_31573_31638[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (13))){
var inst_31558 = (state_31565[(2)]);
var state_31565__$1 = (function (){var statearr_31574 = state_31565;
(statearr_31574[(9)] = inst_31558);

return statearr_31574;
})();
var statearr_31575_31639 = state_31565__$1;
(statearr_31575_31639[(2)] = null);

(statearr_31575_31639[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (6))){
var inst_31530 = (state_31565[(7)]);
var state_31565__$1 = state_31565;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31565__$1,(11),inst_31530);
} else {
if((state_val_31566 === (17))){
var inst_31551 = (state_31565[(2)]);
var state_31565__$1 = state_31565;
if(cljs.core.truth_(inst_31551)){
var statearr_31576_31640 = state_31565__$1;
(statearr_31576_31640[(1)] = (19));

} else {
var statearr_31577_31641 = state_31565__$1;
(statearr_31577_31641[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (3))){
var inst_31563 = (state_31565[(2)]);
var state_31565__$1 = state_31565;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31565__$1,inst_31563);
} else {
if((state_val_31566 === (12))){
var inst_31540 = (state_31565[(10)]);
var state_31565__$1 = state_31565;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31565__$1,(14),inst_31540);
} else {
if((state_val_31566 === (2))){
var state_31565__$1 = state_31565;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31565__$1,(4),results);
} else {
if((state_val_31566 === (19))){
var state_31565__$1 = state_31565;
var statearr_31578_31642 = state_31565__$1;
(statearr_31578_31642[(2)] = null);

(statearr_31578_31642[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (11))){
var inst_31540 = (state_31565[(2)]);
var state_31565__$1 = (function (){var statearr_31579 = state_31565;
(statearr_31579[(10)] = inst_31540);

return statearr_31579;
})();
var statearr_31580_31643 = state_31565__$1;
(statearr_31580_31643[(2)] = null);

(statearr_31580_31643[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (9))){
var state_31565__$1 = state_31565;
var statearr_31581_31644 = state_31565__$1;
(statearr_31581_31644[(2)] = null);

(statearr_31581_31644[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (5))){
var state_31565__$1 = state_31565;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31582_31645 = state_31565__$1;
(statearr_31582_31645[(1)] = (8));

} else {
var statearr_31583_31646 = state_31565__$1;
(statearr_31583_31646[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (14))){
var inst_31545 = (state_31565[(11)]);
var inst_31543 = (state_31565[(8)]);
var inst_31543__$1 = (state_31565[(2)]);
var inst_31544 = (inst_31543__$1 == null);
var inst_31545__$1 = cljs.core.not.call(null,inst_31544);
var state_31565__$1 = (function (){var statearr_31584 = state_31565;
(statearr_31584[(11)] = inst_31545__$1);

(statearr_31584[(8)] = inst_31543__$1);

return statearr_31584;
})();
if(inst_31545__$1){
var statearr_31585_31647 = state_31565__$1;
(statearr_31585_31647[(1)] = (15));

} else {
var statearr_31586_31648 = state_31565__$1;
(statearr_31586_31648[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (16))){
var inst_31545 = (state_31565[(11)]);
var state_31565__$1 = state_31565;
var statearr_31587_31649 = state_31565__$1;
(statearr_31587_31649[(2)] = inst_31545);

(statearr_31587_31649[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (10))){
var inst_31537 = (state_31565[(2)]);
var state_31565__$1 = state_31565;
var statearr_31588_31650 = state_31565__$1;
(statearr_31588_31650[(2)] = inst_31537);

(statearr_31588_31650[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (18))){
var inst_31548 = (state_31565[(2)]);
var state_31565__$1 = state_31565;
var statearr_31589_31651 = state_31565__$1;
(statearr_31589_31651[(2)] = inst_31548);

(statearr_31589_31651[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31566 === (8))){
var inst_31534 = cljs.core.async.close_BANG_.call(null,to);
var state_31565__$1 = state_31565;
var statearr_31590_31652 = state_31565__$1;
(statearr_31590_31652[(2)] = inst_31534);

(statearr_31590_31652[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto__,jobs,results,process,async))
;
return ((function (switch__7585__auto__,c__7600__auto__,jobs,results,process,async){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31594 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31594[(0)] = state_machine__7586__auto__);

(statearr_31594[(1)] = (1));

return statearr_31594;
});
var state_machine__7586__auto____1 = (function (state_31565){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31565);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31595){if((e31595 instanceof Object)){
var ex__7589__auto__ = e31595;
var statearr_31596_31653 = state_31565;
(statearr_31596_31653[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31565);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31595;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31654 = state_31565;
state_31565 = G__31654;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31565){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31565);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto__,jobs,results,process,async))
})();
var state__7602__auto__ = (function (){var statearr_31597 = f__7601__auto__.call(null);
(statearr_31597[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto__);

return statearr_31597;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto__,jobs,results,process,async))
);

return c__7600__auto__;
});
/**
* Takes elements from the from channel and supplies them to the to
* channel, subject to the async function af, with parallelism n. af
* must be a function of two arguments, the first an input value and
* the second a channel on which to place the result(s). af must close!
* the channel before returning.  The presumption is that af will
* return immediately, having launched some asynchronous operation
* whose completion/callback will manipulate the result channel. Outputs
* will be returned in order relative to  the inputs. By default, the to
* channel will be closed when the from channel closes, but can be
* determined by the close?  parameter. Will stop consuming the from
* channel if the to channel closes.
*/
cljs.core.async.pipeline_async = (function() {
var pipeline_async = null;
var pipeline_async__4 = (function (n,to,af,from){
return pipeline_async.call(null,n,to,af,from,true);
});
var pipeline_async__5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});
pipeline_async = function(n,to,af,from,close_QMARK_){
switch(arguments.length){
case 4:
return pipeline_async__4.call(this,n,to,af,from);
case 5:
return pipeline_async__5.call(this,n,to,af,from,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pipeline_async.cljs$core$IFn$_invoke$arity$4 = pipeline_async__4;
pipeline_async.cljs$core$IFn$_invoke$arity$5 = pipeline_async__5;
return pipeline_async;
})()
;
/**
* Takes elements from the from channel and supplies them to the to
* channel, subject to the transducer xf, with parallelism n. Because
* it is parallel, the transducer will be applied independently to each
* element, not across elements, and may produce zero or more outputs
* per input.  Outputs will be returned in order relative to the
* inputs. By default, the to channel will be closed when the from
* channel closes, but can be determined by the close?  parameter. Will
* stop consuming the from channel if the to channel closes.
* 
* Note this is supplied for API compatibility with the Clojure version.
* Values of N > 1 will not result in actual concurrency in a
* single-threaded runtime.
*/
cljs.core.async.pipeline = (function() {
var pipeline = null;
var pipeline__4 = (function (n,to,xf,from){
return pipeline.call(null,n,to,xf,from,true);
});
var pipeline__5 = (function (n,to,xf,from,close_QMARK_){
return pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});
var pipeline__6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});
pipeline = function(n,to,xf,from,close_QMARK_,ex_handler){
switch(arguments.length){
case 4:
return pipeline__4.call(this,n,to,xf,from);
case 5:
return pipeline__5.call(this,n,to,xf,from,close_QMARK_);
case 6:
return pipeline__6.call(this,n,to,xf,from,close_QMARK_,ex_handler);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pipeline.cljs$core$IFn$_invoke$arity$4 = pipeline__4;
pipeline.cljs$core$IFn$_invoke$arity$5 = pipeline__5;
pipeline.cljs$core$IFn$_invoke$arity$6 = pipeline__6;
return pipeline;
})()
;
/**
* Takes a predicate and a source channel and returns a vector of two
* channels, the first of which will contain the values for which the
* predicate returned true, the second those for which it returned
* false.
* 
* The out channels will be unbuffered by default, or two buf-or-ns can
* be supplied. The channels will close after the source channel has
* closed.
*/
cljs.core.async.split = (function() {
var split = null;
var split__2 = (function (p,ch){
return split.call(null,p,ch,null,null);
});
var split__4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__7600__auto___31755 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___31755,tc,fc){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___31755,tc,fc){
return (function (state_31730){
var state_val_31731 = (state_31730[(1)]);
if((state_val_31731 === (7))){
var inst_31726 = (state_31730[(2)]);
var state_31730__$1 = state_31730;
var statearr_31732_31756 = state_31730__$1;
(statearr_31732_31756[(2)] = inst_31726);

(statearr_31732_31756[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (1))){
var state_31730__$1 = state_31730;
var statearr_31733_31757 = state_31730__$1;
(statearr_31733_31757[(2)] = null);

(statearr_31733_31757[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (4))){
var inst_31707 = (state_31730[(7)]);
var inst_31707__$1 = (state_31730[(2)]);
var inst_31708 = (inst_31707__$1 == null);
var state_31730__$1 = (function (){var statearr_31734 = state_31730;
(statearr_31734[(7)] = inst_31707__$1);

return statearr_31734;
})();
if(cljs.core.truth_(inst_31708)){
var statearr_31735_31758 = state_31730__$1;
(statearr_31735_31758[(1)] = (5));

} else {
var statearr_31736_31759 = state_31730__$1;
(statearr_31736_31759[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (13))){
var state_31730__$1 = state_31730;
var statearr_31737_31760 = state_31730__$1;
(statearr_31737_31760[(2)] = null);

(statearr_31737_31760[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (6))){
var inst_31707 = (state_31730[(7)]);
var inst_31713 = p.call(null,inst_31707);
var state_31730__$1 = state_31730;
if(cljs.core.truth_(inst_31713)){
var statearr_31738_31761 = state_31730__$1;
(statearr_31738_31761[(1)] = (9));

} else {
var statearr_31739_31762 = state_31730__$1;
(statearr_31739_31762[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (3))){
var inst_31728 = (state_31730[(2)]);
var state_31730__$1 = state_31730;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31730__$1,inst_31728);
} else {
if((state_val_31731 === (12))){
var state_31730__$1 = state_31730;
var statearr_31740_31763 = state_31730__$1;
(statearr_31740_31763[(2)] = null);

(statearr_31740_31763[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (2))){
var state_31730__$1 = state_31730;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31730__$1,(4),ch);
} else {
if((state_val_31731 === (11))){
var inst_31707 = (state_31730[(7)]);
var inst_31717 = (state_31730[(2)]);
var state_31730__$1 = state_31730;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31730__$1,(8),inst_31717,inst_31707);
} else {
if((state_val_31731 === (9))){
var state_31730__$1 = state_31730;
var statearr_31741_31764 = state_31730__$1;
(statearr_31741_31764[(2)] = tc);

(statearr_31741_31764[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (5))){
var inst_31710 = cljs.core.async.close_BANG_.call(null,tc);
var inst_31711 = cljs.core.async.close_BANG_.call(null,fc);
var state_31730__$1 = (function (){var statearr_31742 = state_31730;
(statearr_31742[(8)] = inst_31710);

return statearr_31742;
})();
var statearr_31743_31765 = state_31730__$1;
(statearr_31743_31765[(2)] = inst_31711);

(statearr_31743_31765[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (14))){
var inst_31724 = (state_31730[(2)]);
var state_31730__$1 = state_31730;
var statearr_31744_31766 = state_31730__$1;
(statearr_31744_31766[(2)] = inst_31724);

(statearr_31744_31766[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (10))){
var state_31730__$1 = state_31730;
var statearr_31745_31767 = state_31730__$1;
(statearr_31745_31767[(2)] = fc);

(statearr_31745_31767[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31731 === (8))){
var inst_31719 = (state_31730[(2)]);
var state_31730__$1 = state_31730;
if(cljs.core.truth_(inst_31719)){
var statearr_31746_31768 = state_31730__$1;
(statearr_31746_31768[(1)] = (12));

} else {
var statearr_31747_31769 = state_31730__$1;
(statearr_31747_31769[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___31755,tc,fc))
;
return ((function (switch__7585__auto__,c__7600__auto___31755,tc,fc){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31751 = [null,null,null,null,null,null,null,null,null];
(statearr_31751[(0)] = state_machine__7586__auto__);

(statearr_31751[(1)] = (1));

return statearr_31751;
});
var state_machine__7586__auto____1 = (function (state_31730){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31730);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31752){if((e31752 instanceof Object)){
var ex__7589__auto__ = e31752;
var statearr_31753_31770 = state_31730;
(statearr_31753_31770[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31730);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31752;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31771 = state_31730;
state_31730 = G__31771;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31730){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31730);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___31755,tc,fc))
})();
var state__7602__auto__ = (function (){var statearr_31754 = f__7601__auto__.call(null);
(statearr_31754[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___31755);

return statearr_31754;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___31755,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});
split = function(p,ch,t_buf_or_n,f_buf_or_n){
switch(arguments.length){
case 2:
return split__2.call(this,p,ch);
case 4:
return split__4.call(this,p,ch,t_buf_or_n,f_buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
split.cljs$core$IFn$_invoke$arity$2 = split__2;
split.cljs$core$IFn$_invoke$arity$4 = split__4;
return split;
})()
;
/**
* f should be a function of 2 arguments. Returns a channel containing
* the single result of applying f to init and the first item from the
* channel, then applying f to that result and the 2nd item, etc. If
* the channel closes without yielding items, returns init and f is not
* called. ch must close before reduce produces a result.
*/
cljs.core.async.reduce = (function reduce(f,init,ch){
var c__7600__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto__){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto__){
return (function (state_31818){
var state_val_31819 = (state_31818[(1)]);
if((state_val_31819 === (7))){
var inst_31814 = (state_31818[(2)]);
var state_31818__$1 = state_31818;
var statearr_31820_31836 = state_31818__$1;
(statearr_31820_31836[(2)] = inst_31814);

(statearr_31820_31836[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31819 === (6))){
var inst_31807 = (state_31818[(7)]);
var inst_31804 = (state_31818[(8)]);
var inst_31811 = f.call(null,inst_31804,inst_31807);
var inst_31804__$1 = inst_31811;
var state_31818__$1 = (function (){var statearr_31821 = state_31818;
(statearr_31821[(8)] = inst_31804__$1);

return statearr_31821;
})();
var statearr_31822_31837 = state_31818__$1;
(statearr_31822_31837[(2)] = null);

(statearr_31822_31837[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31819 === (5))){
var inst_31804 = (state_31818[(8)]);
var state_31818__$1 = state_31818;
var statearr_31823_31838 = state_31818__$1;
(statearr_31823_31838[(2)] = inst_31804);

(statearr_31823_31838[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31819 === (4))){
var inst_31807 = (state_31818[(7)]);
var inst_31807__$1 = (state_31818[(2)]);
var inst_31808 = (inst_31807__$1 == null);
var state_31818__$1 = (function (){var statearr_31824 = state_31818;
(statearr_31824[(7)] = inst_31807__$1);

return statearr_31824;
})();
if(cljs.core.truth_(inst_31808)){
var statearr_31825_31839 = state_31818__$1;
(statearr_31825_31839[(1)] = (5));

} else {
var statearr_31826_31840 = state_31818__$1;
(statearr_31826_31840[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31819 === (3))){
var inst_31816 = (state_31818[(2)]);
var state_31818__$1 = state_31818;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31818__$1,inst_31816);
} else {
if((state_val_31819 === (2))){
var state_31818__$1 = state_31818;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31818__$1,(4),ch);
} else {
if((state_val_31819 === (1))){
var inst_31804 = init;
var state_31818__$1 = (function (){var statearr_31827 = state_31818;
(statearr_31827[(8)] = inst_31804);

return statearr_31827;
})();
var statearr_31828_31841 = state_31818__$1;
(statearr_31828_31841[(2)] = null);

(statearr_31828_31841[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__7600__auto__))
;
return ((function (switch__7585__auto__,c__7600__auto__){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31832 = [null,null,null,null,null,null,null,null,null];
(statearr_31832[(0)] = state_machine__7586__auto__);

(statearr_31832[(1)] = (1));

return statearr_31832;
});
var state_machine__7586__auto____1 = (function (state_31818){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31818);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31833){if((e31833 instanceof Object)){
var ex__7589__auto__ = e31833;
var statearr_31834_31842 = state_31818;
(statearr_31834_31842[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31818);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31833;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31843 = state_31818;
state_31818 = G__31843;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31818){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31818);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto__))
})();
var state__7602__auto__ = (function (){var statearr_31835 = f__7601__auto__.call(null);
(statearr_31835[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto__);

return statearr_31835;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto__))
);

return c__7600__auto__;
});
/**
* Puts the contents of coll into the supplied channel.
* 
* By default the channel will be closed after the items are copied,
* but can be determined by the close? parameter.
* 
* Returns a channel which will close after the items are copied.
*/
cljs.core.async.onto_chan = (function() {
var onto_chan = null;
var onto_chan__2 = (function (ch,coll){
return onto_chan.call(null,ch,coll,true);
});
var onto_chan__3 = (function (ch,coll,close_QMARK_){
var c__7600__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto__){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto__){
return (function (state_31917){
var state_val_31918 = (state_31917[(1)]);
if((state_val_31918 === (7))){
var inst_31899 = (state_31917[(2)]);
var state_31917__$1 = state_31917;
var statearr_31919_31942 = state_31917__$1;
(statearr_31919_31942[(2)] = inst_31899);

(statearr_31919_31942[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (1))){
var inst_31893 = cljs.core.seq.call(null,coll);
var inst_31894 = inst_31893;
var state_31917__$1 = (function (){var statearr_31920 = state_31917;
(statearr_31920[(7)] = inst_31894);

return statearr_31920;
})();
var statearr_31921_31943 = state_31917__$1;
(statearr_31921_31943[(2)] = null);

(statearr_31921_31943[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (4))){
var inst_31894 = (state_31917[(7)]);
var inst_31897 = cljs.core.first.call(null,inst_31894);
var state_31917__$1 = state_31917;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31917__$1,(7),ch,inst_31897);
} else {
if((state_val_31918 === (13))){
var inst_31911 = (state_31917[(2)]);
var state_31917__$1 = state_31917;
var statearr_31922_31944 = state_31917__$1;
(statearr_31922_31944[(2)] = inst_31911);

(statearr_31922_31944[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (6))){
var inst_31902 = (state_31917[(2)]);
var state_31917__$1 = state_31917;
if(cljs.core.truth_(inst_31902)){
var statearr_31923_31945 = state_31917__$1;
(statearr_31923_31945[(1)] = (8));

} else {
var statearr_31924_31946 = state_31917__$1;
(statearr_31924_31946[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (3))){
var inst_31915 = (state_31917[(2)]);
var state_31917__$1 = state_31917;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31917__$1,inst_31915);
} else {
if((state_val_31918 === (12))){
var state_31917__$1 = state_31917;
var statearr_31925_31947 = state_31917__$1;
(statearr_31925_31947[(2)] = null);

(statearr_31925_31947[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (2))){
var inst_31894 = (state_31917[(7)]);
var state_31917__$1 = state_31917;
if(cljs.core.truth_(inst_31894)){
var statearr_31926_31948 = state_31917__$1;
(statearr_31926_31948[(1)] = (4));

} else {
var statearr_31927_31949 = state_31917__$1;
(statearr_31927_31949[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (11))){
var inst_31908 = cljs.core.async.close_BANG_.call(null,ch);
var state_31917__$1 = state_31917;
var statearr_31928_31950 = state_31917__$1;
(statearr_31928_31950[(2)] = inst_31908);

(statearr_31928_31950[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (9))){
var state_31917__$1 = state_31917;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31929_31951 = state_31917__$1;
(statearr_31929_31951[(1)] = (11));

} else {
var statearr_31930_31952 = state_31917__$1;
(statearr_31930_31952[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (5))){
var inst_31894 = (state_31917[(7)]);
var state_31917__$1 = state_31917;
var statearr_31931_31953 = state_31917__$1;
(statearr_31931_31953[(2)] = inst_31894);

(statearr_31931_31953[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (10))){
var inst_31913 = (state_31917[(2)]);
var state_31917__$1 = state_31917;
var statearr_31932_31954 = state_31917__$1;
(statearr_31932_31954[(2)] = inst_31913);

(statearr_31932_31954[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31918 === (8))){
var inst_31894 = (state_31917[(7)]);
var inst_31904 = cljs.core.next.call(null,inst_31894);
var inst_31894__$1 = inst_31904;
var state_31917__$1 = (function (){var statearr_31933 = state_31917;
(statearr_31933[(7)] = inst_31894__$1);

return statearr_31933;
})();
var statearr_31934_31955 = state_31917__$1;
(statearr_31934_31955[(2)] = null);

(statearr_31934_31955[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto__))
;
return ((function (switch__7585__auto__,c__7600__auto__){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_31938 = [null,null,null,null,null,null,null,null];
(statearr_31938[(0)] = state_machine__7586__auto__);

(statearr_31938[(1)] = (1));

return statearr_31938;
});
var state_machine__7586__auto____1 = (function (state_31917){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_31917);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e31939){if((e31939 instanceof Object)){
var ex__7589__auto__ = e31939;
var statearr_31940_31956 = state_31917;
(statearr_31940_31956[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31917);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31939;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31957 = state_31917;
state_31917 = G__31957;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_31917){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_31917);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto__))
})();
var state__7602__auto__ = (function (){var statearr_31941 = f__7601__auto__.call(null);
(statearr_31941[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto__);

return statearr_31941;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto__))
);

return c__7600__auto__;
});
onto_chan = function(ch,coll,close_QMARK_){
switch(arguments.length){
case 2:
return onto_chan__2.call(this,ch,coll);
case 3:
return onto_chan__3.call(this,ch,coll,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
onto_chan.cljs$core$IFn$_invoke$arity$2 = onto_chan__2;
onto_chan.cljs$core$IFn$_invoke$arity$3 = onto_chan__3;
return onto_chan;
})()
;
/**
* Creates and returns a channel which contains the contents of coll,
* closing when exhausted.
*/
cljs.core.async.to_chan = (function to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

cljs.core.async.Mux = (function (){var obj31959 = {};
return obj31959;
})();

cljs.core.async.muxch_STAR_ = (function muxch_STAR_(_){
if((function (){var and__3969__auto__ = _;
if(and__3969__auto__){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1;
} else {
return and__3969__auto__;
}
})()){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__4625__auto__ = (((_ == null))?null:_);
return (function (){var or__3981__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
})().call(null,_);
}
});


cljs.core.async.Mult = (function (){var obj31961 = {};
return obj31961;
})();

cljs.core.async.tap_STAR_ = (function tap_STAR_(m,ch,close_QMARK_){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mult$tap_STAR_$arity$3;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
})().call(null,m,ch,close_QMARK_);
}
});

cljs.core.async.untap_STAR_ = (function untap_STAR_(m,ch){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mult$untap_STAR_$arity$2;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
})().call(null,m,ch);
}
});

cljs.core.async.untap_all_STAR_ = (function untap_all_STAR_(m){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
})().call(null,m);
}
});

/**
* Creates and returns a mult(iple) of the supplied channel. Channels
* containing copies of the channel can be created with 'tap', and
* detached with 'untap'.
* 
* Each item is distributed to all taps in parallel and synchronously,
* i.e. each tap must accept before the next item is distributed. Use
* buffering/windowing to prevent slow taps from holding up the mult.
* 
* Items received when there are no taps get dropped.
* 
* If a tap puts to a closed channel, it will be removed from the mult.
*/
cljs.core.async.mult = (function mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t32183 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32183 = (function (cs,ch,mult,meta32184){
this.cs = cs;
this.ch = ch;
this.mult = mult;
this.meta32184 = meta32184;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32183.prototype.cljs$core$async$Mult$ = true;

cljs.core.async.t32183.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t32183.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t32183.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t32183.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t32183.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t32183.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_32185){
var self__ = this;
var _32185__$1 = this;
return self__.meta32184;
});})(cs))
;

cljs.core.async.t32183.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_32185,meta32184__$1){
var self__ = this;
var _32185__$1 = this;
return (new cljs.core.async.t32183(self__.cs,self__.ch,self__.mult,meta32184__$1));
});})(cs))
;

cljs.core.async.t32183.cljs$lang$type = true;

cljs.core.async.t32183.cljs$lang$ctorStr = "cljs.core.async/t32183";

cljs.core.async.t32183.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t32183");
});})(cs))
;

cljs.core.async.__GT_t32183 = ((function (cs){
return (function __GT_t32183(cs__$1,ch__$1,mult__$1,meta32184){
return (new cljs.core.async.t32183(cs__$1,ch__$1,mult__$1,meta32184));
});})(cs))
;

}

return (new cljs.core.async.t32183(cs,ch,mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__7600__auto___32404 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___32404,cs,m,dchan,dctr,done){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___32404,cs,m,dchan,dctr,done){
return (function (state_32316){
var state_val_32317 = (state_32316[(1)]);
if((state_val_32317 === (7))){
var inst_32312 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32318_32405 = state_32316__$1;
(statearr_32318_32405[(2)] = inst_32312);

(statearr_32318_32405[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (20))){
var inst_32217 = (state_32316[(7)]);
var inst_32227 = cljs.core.first.call(null,inst_32217);
var inst_32228 = cljs.core.nth.call(null,inst_32227,(0),null);
var inst_32229 = cljs.core.nth.call(null,inst_32227,(1),null);
var state_32316__$1 = (function (){var statearr_32319 = state_32316;
(statearr_32319[(8)] = inst_32228);

return statearr_32319;
})();
if(cljs.core.truth_(inst_32229)){
var statearr_32320_32406 = state_32316__$1;
(statearr_32320_32406[(1)] = (22));

} else {
var statearr_32321_32407 = state_32316__$1;
(statearr_32321_32407[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (27))){
var inst_32188 = (state_32316[(9)]);
var inst_32259 = (state_32316[(10)]);
var inst_32264 = (state_32316[(11)]);
var inst_32257 = (state_32316[(12)]);
var inst_32264__$1 = cljs.core._nth.call(null,inst_32257,inst_32259);
var inst_32265 = cljs.core.async.put_BANG_.call(null,inst_32264__$1,inst_32188,done);
var state_32316__$1 = (function (){var statearr_32322 = state_32316;
(statearr_32322[(11)] = inst_32264__$1);

return statearr_32322;
})();
if(cljs.core.truth_(inst_32265)){
var statearr_32323_32408 = state_32316__$1;
(statearr_32323_32408[(1)] = (30));

} else {
var statearr_32324_32409 = state_32316__$1;
(statearr_32324_32409[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (1))){
var state_32316__$1 = state_32316;
var statearr_32325_32410 = state_32316__$1;
(statearr_32325_32410[(2)] = null);

(statearr_32325_32410[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (24))){
var inst_32217 = (state_32316[(7)]);
var inst_32234 = (state_32316[(2)]);
var inst_32235 = cljs.core.next.call(null,inst_32217);
var inst_32197 = inst_32235;
var inst_32198 = null;
var inst_32199 = (0);
var inst_32200 = (0);
var state_32316__$1 = (function (){var statearr_32326 = state_32316;
(statearr_32326[(13)] = inst_32199);

(statearr_32326[(14)] = inst_32234);

(statearr_32326[(15)] = inst_32197);

(statearr_32326[(16)] = inst_32200);

(statearr_32326[(17)] = inst_32198);

return statearr_32326;
})();
var statearr_32327_32411 = state_32316__$1;
(statearr_32327_32411[(2)] = null);

(statearr_32327_32411[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (39))){
var state_32316__$1 = state_32316;
var statearr_32331_32412 = state_32316__$1;
(statearr_32331_32412[(2)] = null);

(statearr_32331_32412[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (4))){
var inst_32188 = (state_32316[(9)]);
var inst_32188__$1 = (state_32316[(2)]);
var inst_32189 = (inst_32188__$1 == null);
var state_32316__$1 = (function (){var statearr_32332 = state_32316;
(statearr_32332[(9)] = inst_32188__$1);

return statearr_32332;
})();
if(cljs.core.truth_(inst_32189)){
var statearr_32333_32413 = state_32316__$1;
(statearr_32333_32413[(1)] = (5));

} else {
var statearr_32334_32414 = state_32316__$1;
(statearr_32334_32414[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (15))){
var inst_32199 = (state_32316[(13)]);
var inst_32197 = (state_32316[(15)]);
var inst_32200 = (state_32316[(16)]);
var inst_32198 = (state_32316[(17)]);
var inst_32213 = (state_32316[(2)]);
var inst_32214 = (inst_32200 + (1));
var tmp32328 = inst_32199;
var tmp32329 = inst_32197;
var tmp32330 = inst_32198;
var inst_32197__$1 = tmp32329;
var inst_32198__$1 = tmp32330;
var inst_32199__$1 = tmp32328;
var inst_32200__$1 = inst_32214;
var state_32316__$1 = (function (){var statearr_32335 = state_32316;
(statearr_32335[(13)] = inst_32199__$1);

(statearr_32335[(15)] = inst_32197__$1);

(statearr_32335[(18)] = inst_32213);

(statearr_32335[(16)] = inst_32200__$1);

(statearr_32335[(17)] = inst_32198__$1);

return statearr_32335;
})();
var statearr_32336_32415 = state_32316__$1;
(statearr_32336_32415[(2)] = null);

(statearr_32336_32415[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (21))){
var inst_32238 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32340_32416 = state_32316__$1;
(statearr_32340_32416[(2)] = inst_32238);

(statearr_32340_32416[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (31))){
var inst_32264 = (state_32316[(11)]);
var inst_32268 = done.call(null,null);
var inst_32269 = cljs.core.async.untap_STAR_.call(null,m,inst_32264);
var state_32316__$1 = (function (){var statearr_32341 = state_32316;
(statearr_32341[(19)] = inst_32268);

return statearr_32341;
})();
var statearr_32342_32417 = state_32316__$1;
(statearr_32342_32417[(2)] = inst_32269);

(statearr_32342_32417[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (32))){
var inst_32258 = (state_32316[(20)]);
var inst_32259 = (state_32316[(10)]);
var inst_32256 = (state_32316[(21)]);
var inst_32257 = (state_32316[(12)]);
var inst_32271 = (state_32316[(2)]);
var inst_32272 = (inst_32259 + (1));
var tmp32337 = inst_32258;
var tmp32338 = inst_32256;
var tmp32339 = inst_32257;
var inst_32256__$1 = tmp32338;
var inst_32257__$1 = tmp32339;
var inst_32258__$1 = tmp32337;
var inst_32259__$1 = inst_32272;
var state_32316__$1 = (function (){var statearr_32343 = state_32316;
(statearr_32343[(20)] = inst_32258__$1);

(statearr_32343[(22)] = inst_32271);

(statearr_32343[(10)] = inst_32259__$1);

(statearr_32343[(21)] = inst_32256__$1);

(statearr_32343[(12)] = inst_32257__$1);

return statearr_32343;
})();
var statearr_32344_32418 = state_32316__$1;
(statearr_32344_32418[(2)] = null);

(statearr_32344_32418[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (40))){
var inst_32284 = (state_32316[(23)]);
var inst_32288 = done.call(null,null);
var inst_32289 = cljs.core.async.untap_STAR_.call(null,m,inst_32284);
var state_32316__$1 = (function (){var statearr_32345 = state_32316;
(statearr_32345[(24)] = inst_32288);

return statearr_32345;
})();
var statearr_32346_32419 = state_32316__$1;
(statearr_32346_32419[(2)] = inst_32289);

(statearr_32346_32419[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (33))){
var inst_32275 = (state_32316[(25)]);
var inst_32277 = cljs.core.chunked_seq_QMARK_.call(null,inst_32275);
var state_32316__$1 = state_32316;
if(inst_32277){
var statearr_32347_32420 = state_32316__$1;
(statearr_32347_32420[(1)] = (36));

} else {
var statearr_32348_32421 = state_32316__$1;
(statearr_32348_32421[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (13))){
var inst_32207 = (state_32316[(26)]);
var inst_32210 = cljs.core.async.close_BANG_.call(null,inst_32207);
var state_32316__$1 = state_32316;
var statearr_32349_32422 = state_32316__$1;
(statearr_32349_32422[(2)] = inst_32210);

(statearr_32349_32422[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (22))){
var inst_32228 = (state_32316[(8)]);
var inst_32231 = cljs.core.async.close_BANG_.call(null,inst_32228);
var state_32316__$1 = state_32316;
var statearr_32350_32423 = state_32316__$1;
(statearr_32350_32423[(2)] = inst_32231);

(statearr_32350_32423[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (36))){
var inst_32275 = (state_32316[(25)]);
var inst_32279 = cljs.core.chunk_first.call(null,inst_32275);
var inst_32280 = cljs.core.chunk_rest.call(null,inst_32275);
var inst_32281 = cljs.core.count.call(null,inst_32279);
var inst_32256 = inst_32280;
var inst_32257 = inst_32279;
var inst_32258 = inst_32281;
var inst_32259 = (0);
var state_32316__$1 = (function (){var statearr_32351 = state_32316;
(statearr_32351[(20)] = inst_32258);

(statearr_32351[(10)] = inst_32259);

(statearr_32351[(21)] = inst_32256);

(statearr_32351[(12)] = inst_32257);

return statearr_32351;
})();
var statearr_32352_32424 = state_32316__$1;
(statearr_32352_32424[(2)] = null);

(statearr_32352_32424[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (41))){
var inst_32275 = (state_32316[(25)]);
var inst_32291 = (state_32316[(2)]);
var inst_32292 = cljs.core.next.call(null,inst_32275);
var inst_32256 = inst_32292;
var inst_32257 = null;
var inst_32258 = (0);
var inst_32259 = (0);
var state_32316__$1 = (function (){var statearr_32353 = state_32316;
(statearr_32353[(20)] = inst_32258);

(statearr_32353[(27)] = inst_32291);

(statearr_32353[(10)] = inst_32259);

(statearr_32353[(21)] = inst_32256);

(statearr_32353[(12)] = inst_32257);

return statearr_32353;
})();
var statearr_32354_32425 = state_32316__$1;
(statearr_32354_32425[(2)] = null);

(statearr_32354_32425[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (43))){
var state_32316__$1 = state_32316;
var statearr_32355_32426 = state_32316__$1;
(statearr_32355_32426[(2)] = null);

(statearr_32355_32426[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (29))){
var inst_32300 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32356_32427 = state_32316__$1;
(statearr_32356_32427[(2)] = inst_32300);

(statearr_32356_32427[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (44))){
var inst_32309 = (state_32316[(2)]);
var state_32316__$1 = (function (){var statearr_32357 = state_32316;
(statearr_32357[(28)] = inst_32309);

return statearr_32357;
})();
var statearr_32358_32428 = state_32316__$1;
(statearr_32358_32428[(2)] = null);

(statearr_32358_32428[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (6))){
var inst_32248 = (state_32316[(29)]);
var inst_32247 = cljs.core.deref.call(null,cs);
var inst_32248__$1 = cljs.core.keys.call(null,inst_32247);
var inst_32249 = cljs.core.count.call(null,inst_32248__$1);
var inst_32250 = cljs.core.reset_BANG_.call(null,dctr,inst_32249);
var inst_32255 = cljs.core.seq.call(null,inst_32248__$1);
var inst_32256 = inst_32255;
var inst_32257 = null;
var inst_32258 = (0);
var inst_32259 = (0);
var state_32316__$1 = (function (){var statearr_32359 = state_32316;
(statearr_32359[(20)] = inst_32258);

(statearr_32359[(29)] = inst_32248__$1);

(statearr_32359[(10)] = inst_32259);

(statearr_32359[(21)] = inst_32256);

(statearr_32359[(30)] = inst_32250);

(statearr_32359[(12)] = inst_32257);

return statearr_32359;
})();
var statearr_32360_32429 = state_32316__$1;
(statearr_32360_32429[(2)] = null);

(statearr_32360_32429[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (28))){
var inst_32275 = (state_32316[(25)]);
var inst_32256 = (state_32316[(21)]);
var inst_32275__$1 = cljs.core.seq.call(null,inst_32256);
var state_32316__$1 = (function (){var statearr_32361 = state_32316;
(statearr_32361[(25)] = inst_32275__$1);

return statearr_32361;
})();
if(inst_32275__$1){
var statearr_32362_32430 = state_32316__$1;
(statearr_32362_32430[(1)] = (33));

} else {
var statearr_32363_32431 = state_32316__$1;
(statearr_32363_32431[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (25))){
var inst_32258 = (state_32316[(20)]);
var inst_32259 = (state_32316[(10)]);
var inst_32261 = (inst_32259 < inst_32258);
var inst_32262 = inst_32261;
var state_32316__$1 = state_32316;
if(cljs.core.truth_(inst_32262)){
var statearr_32364_32432 = state_32316__$1;
(statearr_32364_32432[(1)] = (27));

} else {
var statearr_32365_32433 = state_32316__$1;
(statearr_32365_32433[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (34))){
var state_32316__$1 = state_32316;
var statearr_32366_32434 = state_32316__$1;
(statearr_32366_32434[(2)] = null);

(statearr_32366_32434[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (17))){
var state_32316__$1 = state_32316;
var statearr_32367_32435 = state_32316__$1;
(statearr_32367_32435[(2)] = null);

(statearr_32367_32435[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (3))){
var inst_32314 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32316__$1,inst_32314);
} else {
if((state_val_32317 === (12))){
var inst_32243 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32368_32436 = state_32316__$1;
(statearr_32368_32436[(2)] = inst_32243);

(statearr_32368_32436[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (2))){
var state_32316__$1 = state_32316;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32316__$1,(4),ch);
} else {
if((state_val_32317 === (23))){
var state_32316__$1 = state_32316;
var statearr_32369_32437 = state_32316__$1;
(statearr_32369_32437[(2)] = null);

(statearr_32369_32437[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (35))){
var inst_32298 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32370_32438 = state_32316__$1;
(statearr_32370_32438[(2)] = inst_32298);

(statearr_32370_32438[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (19))){
var inst_32217 = (state_32316[(7)]);
var inst_32221 = cljs.core.chunk_first.call(null,inst_32217);
var inst_32222 = cljs.core.chunk_rest.call(null,inst_32217);
var inst_32223 = cljs.core.count.call(null,inst_32221);
var inst_32197 = inst_32222;
var inst_32198 = inst_32221;
var inst_32199 = inst_32223;
var inst_32200 = (0);
var state_32316__$1 = (function (){var statearr_32371 = state_32316;
(statearr_32371[(13)] = inst_32199);

(statearr_32371[(15)] = inst_32197);

(statearr_32371[(16)] = inst_32200);

(statearr_32371[(17)] = inst_32198);

return statearr_32371;
})();
var statearr_32372_32439 = state_32316__$1;
(statearr_32372_32439[(2)] = null);

(statearr_32372_32439[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (11))){
var inst_32217 = (state_32316[(7)]);
var inst_32197 = (state_32316[(15)]);
var inst_32217__$1 = cljs.core.seq.call(null,inst_32197);
var state_32316__$1 = (function (){var statearr_32373 = state_32316;
(statearr_32373[(7)] = inst_32217__$1);

return statearr_32373;
})();
if(inst_32217__$1){
var statearr_32374_32440 = state_32316__$1;
(statearr_32374_32440[(1)] = (16));

} else {
var statearr_32375_32441 = state_32316__$1;
(statearr_32375_32441[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (9))){
var inst_32245 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32376_32442 = state_32316__$1;
(statearr_32376_32442[(2)] = inst_32245);

(statearr_32376_32442[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (5))){
var inst_32195 = cljs.core.deref.call(null,cs);
var inst_32196 = cljs.core.seq.call(null,inst_32195);
var inst_32197 = inst_32196;
var inst_32198 = null;
var inst_32199 = (0);
var inst_32200 = (0);
var state_32316__$1 = (function (){var statearr_32377 = state_32316;
(statearr_32377[(13)] = inst_32199);

(statearr_32377[(15)] = inst_32197);

(statearr_32377[(16)] = inst_32200);

(statearr_32377[(17)] = inst_32198);

return statearr_32377;
})();
var statearr_32378_32443 = state_32316__$1;
(statearr_32378_32443[(2)] = null);

(statearr_32378_32443[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (14))){
var state_32316__$1 = state_32316;
var statearr_32379_32444 = state_32316__$1;
(statearr_32379_32444[(2)] = null);

(statearr_32379_32444[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (45))){
var inst_32306 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32380_32445 = state_32316__$1;
(statearr_32380_32445[(2)] = inst_32306);

(statearr_32380_32445[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (26))){
var inst_32248 = (state_32316[(29)]);
var inst_32302 = (state_32316[(2)]);
var inst_32303 = cljs.core.seq.call(null,inst_32248);
var state_32316__$1 = (function (){var statearr_32381 = state_32316;
(statearr_32381[(31)] = inst_32302);

return statearr_32381;
})();
if(inst_32303){
var statearr_32382_32446 = state_32316__$1;
(statearr_32382_32446[(1)] = (42));

} else {
var statearr_32383_32447 = state_32316__$1;
(statearr_32383_32447[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (16))){
var inst_32217 = (state_32316[(7)]);
var inst_32219 = cljs.core.chunked_seq_QMARK_.call(null,inst_32217);
var state_32316__$1 = state_32316;
if(inst_32219){
var statearr_32384_32448 = state_32316__$1;
(statearr_32384_32448[(1)] = (19));

} else {
var statearr_32385_32449 = state_32316__$1;
(statearr_32385_32449[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (38))){
var inst_32295 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32386_32450 = state_32316__$1;
(statearr_32386_32450[(2)] = inst_32295);

(statearr_32386_32450[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (30))){
var state_32316__$1 = state_32316;
var statearr_32387_32451 = state_32316__$1;
(statearr_32387_32451[(2)] = null);

(statearr_32387_32451[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (10))){
var inst_32200 = (state_32316[(16)]);
var inst_32198 = (state_32316[(17)]);
var inst_32206 = cljs.core._nth.call(null,inst_32198,inst_32200);
var inst_32207 = cljs.core.nth.call(null,inst_32206,(0),null);
var inst_32208 = cljs.core.nth.call(null,inst_32206,(1),null);
var state_32316__$1 = (function (){var statearr_32388 = state_32316;
(statearr_32388[(26)] = inst_32207);

return statearr_32388;
})();
if(cljs.core.truth_(inst_32208)){
var statearr_32389_32452 = state_32316__$1;
(statearr_32389_32452[(1)] = (13));

} else {
var statearr_32390_32453 = state_32316__$1;
(statearr_32390_32453[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (18))){
var inst_32241 = (state_32316[(2)]);
var state_32316__$1 = state_32316;
var statearr_32391_32454 = state_32316__$1;
(statearr_32391_32454[(2)] = inst_32241);

(statearr_32391_32454[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (42))){
var state_32316__$1 = state_32316;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32316__$1,(45),dchan);
} else {
if((state_val_32317 === (37))){
var inst_32284 = (state_32316[(23)]);
var inst_32188 = (state_32316[(9)]);
var inst_32275 = (state_32316[(25)]);
var inst_32284__$1 = cljs.core.first.call(null,inst_32275);
var inst_32285 = cljs.core.async.put_BANG_.call(null,inst_32284__$1,inst_32188,done);
var state_32316__$1 = (function (){var statearr_32392 = state_32316;
(statearr_32392[(23)] = inst_32284__$1);

return statearr_32392;
})();
if(cljs.core.truth_(inst_32285)){
var statearr_32393_32455 = state_32316__$1;
(statearr_32393_32455[(1)] = (39));

} else {
var statearr_32394_32456 = state_32316__$1;
(statearr_32394_32456[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32317 === (8))){
var inst_32199 = (state_32316[(13)]);
var inst_32200 = (state_32316[(16)]);
var inst_32202 = (inst_32200 < inst_32199);
var inst_32203 = inst_32202;
var state_32316__$1 = state_32316;
if(cljs.core.truth_(inst_32203)){
var statearr_32395_32457 = state_32316__$1;
(statearr_32395_32457[(1)] = (10));

} else {
var statearr_32396_32458 = state_32316__$1;
(statearr_32396_32458[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___32404,cs,m,dchan,dctr,done))
;
return ((function (switch__7585__auto__,c__7600__auto___32404,cs,m,dchan,dctr,done){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_32400 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32400[(0)] = state_machine__7586__auto__);

(statearr_32400[(1)] = (1));

return statearr_32400;
});
var state_machine__7586__auto____1 = (function (state_32316){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_32316);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e32401){if((e32401 instanceof Object)){
var ex__7589__auto__ = e32401;
var statearr_32402_32459 = state_32316;
(statearr_32402_32459[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32316);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32401;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32460 = state_32316;
state_32316 = G__32460;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_32316){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_32316);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___32404,cs,m,dchan,dctr,done))
})();
var state__7602__auto__ = (function (){var statearr_32403 = f__7601__auto__.call(null);
(statearr_32403[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___32404);

return statearr_32403;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___32404,cs,m,dchan,dctr,done))
);


return m;
});
/**
* Copies the mult source onto the supplied channel.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.tap = (function() {
var tap = null;
var tap__2 = (function (mult,ch){
return tap.call(null,mult,ch,true);
});
var tap__3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});
tap = function(mult,ch,close_QMARK_){
switch(arguments.length){
case 2:
return tap__2.call(this,mult,ch);
case 3:
return tap__3.call(this,mult,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
tap.cljs$core$IFn$_invoke$arity$2 = tap__2;
tap.cljs$core$IFn$_invoke$arity$3 = tap__3;
return tap;
})()
;
/**
* Disconnects a target channel from a mult
*/
cljs.core.async.untap = (function untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
* Disconnects all target channels from a mult
*/
cljs.core.async.untap_all = (function untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

cljs.core.async.Mix = (function (){var obj32462 = {};
return obj32462;
})();

cljs.core.async.admix_STAR_ = (function admix_STAR_(m,ch){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mix$admix_STAR_$arity$2;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
})().call(null,m,ch);
}
});

cljs.core.async.unmix_STAR_ = (function unmix_STAR_(m,ch){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
})().call(null,m,ch);
}
});

cljs.core.async.unmix_all_STAR_ = (function unmix_all_STAR_(m){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
})().call(null,m);
}
});

cljs.core.async.toggle_STAR_ = (function toggle_STAR_(m,state_map){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
})().call(null,m,state_map);
}
});

cljs.core.async.solo_mode_STAR_ = (function solo_mode_STAR_(m,mode){
if((function (){var and__3969__auto__ = m;
if(and__3969__auto__){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2;
} else {
return and__3969__auto__;
}
})()){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__4625__auto__ = (((m == null))?null:m);
return (function (){var or__3981__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
})().call(null,m,mode);
}
});

/**
* @param {...*} var_args
*/
cljs.core.async.ioc_alts_BANG_ = (function() { 
var ioc_alts_BANG___delegate = function (state,cont_block,ports,p__32463){
var map__32468 = p__32463;
var map__32468__$1 = ((cljs.core.seq_QMARK_.call(null,map__32468))?cljs.core.apply.call(null,cljs.core.hash_map,map__32468):map__32468);
var opts = map__32468__$1;
var statearr_32469_32472 = state;
(statearr_32469_32472[cljs.core.async.impl.ioc_helpers.STATE_IDX] = cont_block);


var temp__4126__auto__ = cljs.core.async.do_alts.call(null,((function (map__32468,map__32468__$1,opts){
return (function (val){
var statearr_32470_32473 = state;
(statearr_32470_32473[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__32468,map__32468__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4126__auto__)){
var cb = temp__4126__auto__;
var statearr_32471_32474 = state;
(statearr_32471_32474[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
};
var ioc_alts_BANG_ = function (state,cont_block,ports,var_args){
var p__32463 = null;
if (arguments.length > 3) {
var G__32475__i = 0, G__32475__a = new Array(arguments.length -  3);
while (G__32475__i < G__32475__a.length) {G__32475__a[G__32475__i] = arguments[G__32475__i + 3]; ++G__32475__i;}
  p__32463 = new cljs.core.IndexedSeq(G__32475__a,0);
} 
return ioc_alts_BANG___delegate.call(this,state,cont_block,ports,p__32463);};
ioc_alts_BANG_.cljs$lang$maxFixedArity = 3;
ioc_alts_BANG_.cljs$lang$applyTo = (function (arglist__32476){
var state = cljs.core.first(arglist__32476);
arglist__32476 = cljs.core.next(arglist__32476);
var cont_block = cljs.core.first(arglist__32476);
arglist__32476 = cljs.core.next(arglist__32476);
var ports = cljs.core.first(arglist__32476);
var p__32463 = cljs.core.rest(arglist__32476);
return ioc_alts_BANG___delegate(state,cont_block,ports,p__32463);
});
ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = ioc_alts_BANG___delegate;
return ioc_alts_BANG_;
})()
;
/**
* Creates and returns a mix of one or more input channels which will
* be put on the supplied out channel. Input sources can be added to
* the mix with 'admix', and removed with 'unmix'. A mix supports
* soloing, muting and pausing multiple inputs atomically using
* 'toggle', and can solo using either muting or pausing as determined
* by 'solo-mode'.
* 
* Each channel can have zero or more boolean modes set via 'toggle':
* 
* :solo - when true, only this (ond other soloed) channel(s) will appear
* in the mix output channel. :mute and :pause states of soloed
* channels are ignored. If solo-mode is :mute, non-soloed
* channels are muted, if :pause, non-soloed channels are
* paused.
* 
* :mute - muted channels will have their contents consumed but not included in the mix
* :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
*/
cljs.core.async.mix = (function mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t32596 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32596 = (function (change,mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta32597){
this.change = change;
this.mix = mix;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta32597 = meta32597;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32596.prototype.cljs$core$async$Mix$ = true;

cljs.core.async.t32596.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("mode must be one of: "),cljs.core.str(self__.solo_modes)].join('')),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"mode","mode",-2000032078,null))))].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t32596.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_32598){
var self__ = this;
var _32598__$1 = this;
return self__.meta32597;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_32598,meta32597__$1){
var self__ = this;
var _32598__$1 = this;
return (new cljs.core.async.t32596(self__.change,self__.mix,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta32597__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t32596.cljs$lang$type = true;

cljs.core.async.t32596.cljs$lang$ctorStr = "cljs.core.async/t32596";

cljs.core.async.t32596.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t32596");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t32596 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function __GT_t32596(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta32597){
return (new cljs.core.async.t32596(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta32597));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t32596(change,mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__7600__auto___32715 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___32715,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___32715,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_32668){
var state_val_32669 = (state_32668[(1)]);
if((state_val_32669 === (7))){
var inst_32612 = (state_32668[(7)]);
var inst_32617 = cljs.core.apply.call(null,cljs.core.hash_map,inst_32612);
var state_32668__$1 = state_32668;
var statearr_32670_32716 = state_32668__$1;
(statearr_32670_32716[(2)] = inst_32617);

(statearr_32670_32716[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (20))){
var inst_32627 = (state_32668[(8)]);
var state_32668__$1 = state_32668;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32668__$1,(23),out,inst_32627);
} else {
if((state_val_32669 === (1))){
var inst_32602 = (state_32668[(9)]);
var inst_32602__$1 = calc_state.call(null);
var inst_32603 = cljs.core.seq_QMARK_.call(null,inst_32602__$1);
var state_32668__$1 = (function (){var statearr_32671 = state_32668;
(statearr_32671[(9)] = inst_32602__$1);

return statearr_32671;
})();
if(inst_32603){
var statearr_32672_32717 = state_32668__$1;
(statearr_32672_32717[(1)] = (2));

} else {
var statearr_32673_32718 = state_32668__$1;
(statearr_32673_32718[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (24))){
var inst_32620 = (state_32668[(10)]);
var inst_32612 = inst_32620;
var state_32668__$1 = (function (){var statearr_32674 = state_32668;
(statearr_32674[(7)] = inst_32612);

return statearr_32674;
})();
var statearr_32675_32719 = state_32668__$1;
(statearr_32675_32719[(2)] = null);

(statearr_32675_32719[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (4))){
var inst_32602 = (state_32668[(9)]);
var inst_32608 = (state_32668[(2)]);
var inst_32609 = cljs.core.get.call(null,inst_32608,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_32610 = cljs.core.get.call(null,inst_32608,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32611 = cljs.core.get.call(null,inst_32608,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32612 = inst_32602;
var state_32668__$1 = (function (){var statearr_32676 = state_32668;
(statearr_32676[(11)] = inst_32609);

(statearr_32676[(7)] = inst_32612);

(statearr_32676[(12)] = inst_32610);

(statearr_32676[(13)] = inst_32611);

return statearr_32676;
})();
var statearr_32677_32720 = state_32668__$1;
(statearr_32677_32720[(2)] = null);

(statearr_32677_32720[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (15))){
var state_32668__$1 = state_32668;
var statearr_32678_32721 = state_32668__$1;
(statearr_32678_32721[(2)] = null);

(statearr_32678_32721[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (21))){
var inst_32620 = (state_32668[(10)]);
var inst_32612 = inst_32620;
var state_32668__$1 = (function (){var statearr_32679 = state_32668;
(statearr_32679[(7)] = inst_32612);

return statearr_32679;
})();
var statearr_32680_32722 = state_32668__$1;
(statearr_32680_32722[(2)] = null);

(statearr_32680_32722[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (13))){
var inst_32664 = (state_32668[(2)]);
var state_32668__$1 = state_32668;
var statearr_32681_32723 = state_32668__$1;
(statearr_32681_32723[(2)] = inst_32664);

(statearr_32681_32723[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (22))){
var inst_32662 = (state_32668[(2)]);
var state_32668__$1 = state_32668;
var statearr_32682_32724 = state_32668__$1;
(statearr_32682_32724[(2)] = inst_32662);

(statearr_32682_32724[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (6))){
var inst_32666 = (state_32668[(2)]);
var state_32668__$1 = state_32668;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32668__$1,inst_32666);
} else {
if((state_val_32669 === (25))){
var state_32668__$1 = state_32668;
var statearr_32683_32725 = state_32668__$1;
(statearr_32683_32725[(2)] = null);

(statearr_32683_32725[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (17))){
var inst_32642 = (state_32668[(14)]);
var state_32668__$1 = state_32668;
var statearr_32684_32726 = state_32668__$1;
(statearr_32684_32726[(2)] = inst_32642);

(statearr_32684_32726[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (3))){
var inst_32602 = (state_32668[(9)]);
var state_32668__$1 = state_32668;
var statearr_32685_32727 = state_32668__$1;
(statearr_32685_32727[(2)] = inst_32602);

(statearr_32685_32727[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (12))){
var inst_32642 = (state_32668[(14)]);
var inst_32623 = (state_32668[(15)]);
var inst_32628 = (state_32668[(16)]);
var inst_32642__$1 = inst_32623.call(null,inst_32628);
var state_32668__$1 = (function (){var statearr_32686 = state_32668;
(statearr_32686[(14)] = inst_32642__$1);

return statearr_32686;
})();
if(cljs.core.truth_(inst_32642__$1)){
var statearr_32687_32728 = state_32668__$1;
(statearr_32687_32728[(1)] = (17));

} else {
var statearr_32688_32729 = state_32668__$1;
(statearr_32688_32729[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (2))){
var inst_32602 = (state_32668[(9)]);
var inst_32605 = cljs.core.apply.call(null,cljs.core.hash_map,inst_32602);
var state_32668__$1 = state_32668;
var statearr_32689_32730 = state_32668__$1;
(statearr_32689_32730[(2)] = inst_32605);

(statearr_32689_32730[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (23))){
var inst_32653 = (state_32668[(2)]);
var state_32668__$1 = state_32668;
if(cljs.core.truth_(inst_32653)){
var statearr_32690_32731 = state_32668__$1;
(statearr_32690_32731[(1)] = (24));

} else {
var statearr_32691_32732 = state_32668__$1;
(statearr_32691_32732[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (19))){
var inst_32650 = (state_32668[(2)]);
var state_32668__$1 = state_32668;
if(cljs.core.truth_(inst_32650)){
var statearr_32692_32733 = state_32668__$1;
(statearr_32692_32733[(1)] = (20));

} else {
var statearr_32693_32734 = state_32668__$1;
(statearr_32693_32734[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (11))){
var inst_32627 = (state_32668[(8)]);
var inst_32633 = (inst_32627 == null);
var state_32668__$1 = state_32668;
if(cljs.core.truth_(inst_32633)){
var statearr_32694_32735 = state_32668__$1;
(statearr_32694_32735[(1)] = (14));

} else {
var statearr_32695_32736 = state_32668__$1;
(statearr_32695_32736[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (9))){
var inst_32620 = (state_32668[(10)]);
var inst_32620__$1 = (state_32668[(2)]);
var inst_32621 = cljs.core.get.call(null,inst_32620__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_32622 = cljs.core.get.call(null,inst_32620__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32623 = cljs.core.get.call(null,inst_32620__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var state_32668__$1 = (function (){var statearr_32696 = state_32668;
(statearr_32696[(10)] = inst_32620__$1);

(statearr_32696[(15)] = inst_32623);

(statearr_32696[(17)] = inst_32622);

return statearr_32696;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_32668__$1,(10),inst_32621);
} else {
if((state_val_32669 === (5))){
var inst_32612 = (state_32668[(7)]);
var inst_32615 = cljs.core.seq_QMARK_.call(null,inst_32612);
var state_32668__$1 = state_32668;
if(inst_32615){
var statearr_32697_32737 = state_32668__$1;
(statearr_32697_32737[(1)] = (7));

} else {
var statearr_32698_32738 = state_32668__$1;
(statearr_32698_32738[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (14))){
var inst_32628 = (state_32668[(16)]);
var inst_32635 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_32628);
var state_32668__$1 = state_32668;
var statearr_32699_32739 = state_32668__$1;
(statearr_32699_32739[(2)] = inst_32635);

(statearr_32699_32739[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (26))){
var inst_32658 = (state_32668[(2)]);
var state_32668__$1 = state_32668;
var statearr_32700_32740 = state_32668__$1;
(statearr_32700_32740[(2)] = inst_32658);

(statearr_32700_32740[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (16))){
var inst_32638 = (state_32668[(2)]);
var inst_32639 = calc_state.call(null);
var inst_32612 = inst_32639;
var state_32668__$1 = (function (){var statearr_32701 = state_32668;
(statearr_32701[(18)] = inst_32638);

(statearr_32701[(7)] = inst_32612);

return statearr_32701;
})();
var statearr_32702_32741 = state_32668__$1;
(statearr_32702_32741[(2)] = null);

(statearr_32702_32741[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (10))){
var inst_32627 = (state_32668[(8)]);
var inst_32628 = (state_32668[(16)]);
var inst_32626 = (state_32668[(2)]);
var inst_32627__$1 = cljs.core.nth.call(null,inst_32626,(0),null);
var inst_32628__$1 = cljs.core.nth.call(null,inst_32626,(1),null);
var inst_32629 = (inst_32627__$1 == null);
var inst_32630 = cljs.core._EQ_.call(null,inst_32628__$1,change);
var inst_32631 = (inst_32629) || (inst_32630);
var state_32668__$1 = (function (){var statearr_32703 = state_32668;
(statearr_32703[(8)] = inst_32627__$1);

(statearr_32703[(16)] = inst_32628__$1);

return statearr_32703;
})();
if(cljs.core.truth_(inst_32631)){
var statearr_32704_32742 = state_32668__$1;
(statearr_32704_32742[(1)] = (11));

} else {
var statearr_32705_32743 = state_32668__$1;
(statearr_32705_32743[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (18))){
var inst_32623 = (state_32668[(15)]);
var inst_32622 = (state_32668[(17)]);
var inst_32628 = (state_32668[(16)]);
var inst_32645 = cljs.core.empty_QMARK_.call(null,inst_32623);
var inst_32646 = inst_32622.call(null,inst_32628);
var inst_32647 = cljs.core.not.call(null,inst_32646);
var inst_32648 = (inst_32645) && (inst_32647);
var state_32668__$1 = state_32668;
var statearr_32706_32744 = state_32668__$1;
(statearr_32706_32744[(2)] = inst_32648);

(statearr_32706_32744[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32669 === (8))){
var inst_32612 = (state_32668[(7)]);
var state_32668__$1 = state_32668;
var statearr_32707_32745 = state_32668__$1;
(statearr_32707_32745[(2)] = inst_32612);

(statearr_32707_32745[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___32715,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__7585__auto__,c__7600__auto___32715,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_32711 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32711[(0)] = state_machine__7586__auto__);

(statearr_32711[(1)] = (1));

return statearr_32711;
});
var state_machine__7586__auto____1 = (function (state_32668){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_32668);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e32712){if((e32712 instanceof Object)){
var ex__7589__auto__ = e32712;
var statearr_32713_32746 = state_32668;
(statearr_32713_32746[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32668);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32712;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32747 = state_32668;
state_32668 = G__32747;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_32668){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_32668);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___32715,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__7602__auto__ = (function (){var statearr_32714 = f__7601__auto__.call(null);
(statearr_32714[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___32715);

return statearr_32714;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___32715,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
* Adds ch as an input to the mix
*/
cljs.core.async.admix = (function admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
* Removes ch as an input to the mix
*/
cljs.core.async.unmix = (function unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
* removes all inputs from the mix
*/
cljs.core.async.unmix_all = (function unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
* Atomically sets the state(s) of one or more channels in a mix. The
* state map is a map of channels -> channel-state-map. A
* channel-state-map is a map of attrs -> boolean, where attr is one or
* more of :mute, :pause or :solo. Any states supplied are merged with
* the current state.
* 
* Note that channels can be added to a mix via toggle, which can be
* used to add channels in a particular (e.g. paused) state.
*/
cljs.core.async.toggle = (function toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
* Sets the solo mode of the mix. mode must be one of :mute or :pause
*/
cljs.core.async.solo_mode = (function solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

cljs.core.async.Pub = (function (){var obj32749 = {};
return obj32749;
})();

cljs.core.async.sub_STAR_ = (function sub_STAR_(p,v,ch,close_QMARK_){
if((function (){var and__3969__auto__ = p;
if(and__3969__auto__){
return p.cljs$core$async$Pub$sub_STAR_$arity$4;
} else {
return and__3969__auto__;
}
})()){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__4625__auto__ = (((p == null))?null:p);
return (function (){var or__3981__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
})().call(null,p,v,ch,close_QMARK_);
}
});

cljs.core.async.unsub_STAR_ = (function unsub_STAR_(p,v,ch){
if((function (){var and__3969__auto__ = p;
if(and__3969__auto__){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3;
} else {
return and__3969__auto__;
}
})()){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__4625__auto__ = (((p == null))?null:p);
return (function (){var or__3981__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
})().call(null,p,v,ch);
}
});

cljs.core.async.unsub_all_STAR_ = (function() {
var unsub_all_STAR_ = null;
var unsub_all_STAR___1 = (function (p){
if((function (){var and__3969__auto__ = p;
if(and__3969__auto__){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1;
} else {
return and__3969__auto__;
}
})()){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__4625__auto__ = (((p == null))?null:p);
return (function (){var or__3981__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p);
}
});
var unsub_all_STAR___2 = (function (p,v){
if((function (){var and__3969__auto__ = p;
if(and__3969__auto__){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2;
} else {
return and__3969__auto__;
}
})()){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__4625__auto__ = (((p == null))?null:p);
return (function (){var or__3981__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4625__auto__)]);
if(or__3981__auto__){
return or__3981__auto__;
} else {
var or__3981__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(or__3981__auto____$1){
return or__3981__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p,v);
}
});
unsub_all_STAR_ = function(p,v){
switch(arguments.length){
case 1:
return unsub_all_STAR___1.call(this,p);
case 2:
return unsub_all_STAR___2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = unsub_all_STAR___1;
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = unsub_all_STAR___2;
return unsub_all_STAR_;
})()
;

/**
* Creates and returns a pub(lication) of the supplied channel,
* partitioned into topics by the topic-fn. topic-fn will be applied to
* each value on the channel and the result will determine the 'topic'
* on which that value will be put. Channels can be subscribed to
* receive copies of topics using 'sub', and unsubscribed using
* 'unsub'. Each topic will be handled by an internal mult on a
* dedicated channel. By default these internal channels are
* unbuffered, but a buf-fn can be supplied which, given a topic,
* creates a buffer with desired properties.
* 
* Each item is distributed to all subs in parallel and synchronously,
* i.e. each sub must accept before the next item is distributed. Use
* buffering/windowing to prevent slow subs from holding up the pub.
* 
* Items received when there are no matching subs get dropped.
* 
* Note that if buf-fns are used then each topic is handled
* asynchronously, i.e. if a channel is subscribed to more than one
* topic it should not expect them to be interleaved identically with
* the source.
*/
cljs.core.async.pub = (function() {
var pub = null;
var pub__2 = (function (ch,topic_fn){
return pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});
var pub__3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__3981__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__3981__auto__)){
return or__3981__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__3981__auto__,mults){
return (function (p1__32750_SHARP_){
if(cljs.core.truth_(p1__32750_SHARP_.call(null,topic))){
return p1__32750_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__32750_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__3981__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t32873 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32873 = (function (ensure_mult,mults,buf_fn,topic_fn,ch,pub,meta32874){
this.ensure_mult = ensure_mult;
this.mults = mults;
this.buf_fn = buf_fn;
this.topic_fn = topic_fn;
this.ch = ch;
this.pub = pub;
this.meta32874 = meta32874;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32873.prototype.cljs$core$async$Pub$ = true;

cljs.core.async.t32873.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t32873.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4126__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4126__auto__)){
var m = temp__4126__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t32873.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t32873.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t32873.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t32873.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t32873.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_32875){
var self__ = this;
var _32875__$1 = this;
return self__.meta32874;
});})(mults,ensure_mult))
;

cljs.core.async.t32873.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_32875,meta32874__$1){
var self__ = this;
var _32875__$1 = this;
return (new cljs.core.async.t32873(self__.ensure_mult,self__.mults,self__.buf_fn,self__.topic_fn,self__.ch,self__.pub,meta32874__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t32873.cljs$lang$type = true;

cljs.core.async.t32873.cljs$lang$ctorStr = "cljs.core.async/t32873";

cljs.core.async.t32873.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t32873");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t32873 = ((function (mults,ensure_mult){
return (function __GT_t32873(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta32874){
return (new cljs.core.async.t32873(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta32874));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t32873(ensure_mult,mults,buf_fn,topic_fn,ch,pub,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__7600__auto___32995 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___32995,mults,ensure_mult,p){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___32995,mults,ensure_mult,p){
return (function (state_32947){
var state_val_32948 = (state_32947[(1)]);
if((state_val_32948 === (7))){
var inst_32943 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
var statearr_32949_32996 = state_32947__$1;
(statearr_32949_32996[(2)] = inst_32943);

(statearr_32949_32996[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (20))){
var state_32947__$1 = state_32947;
var statearr_32950_32997 = state_32947__$1;
(statearr_32950_32997[(2)] = null);

(statearr_32950_32997[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (1))){
var state_32947__$1 = state_32947;
var statearr_32951_32998 = state_32947__$1;
(statearr_32951_32998[(2)] = null);

(statearr_32951_32998[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (24))){
var inst_32926 = (state_32947[(7)]);
var inst_32935 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_32926);
var state_32947__$1 = state_32947;
var statearr_32952_32999 = state_32947__$1;
(statearr_32952_32999[(2)] = inst_32935);

(statearr_32952_32999[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (4))){
var inst_32878 = (state_32947[(8)]);
var inst_32878__$1 = (state_32947[(2)]);
var inst_32879 = (inst_32878__$1 == null);
var state_32947__$1 = (function (){var statearr_32953 = state_32947;
(statearr_32953[(8)] = inst_32878__$1);

return statearr_32953;
})();
if(cljs.core.truth_(inst_32879)){
var statearr_32954_33000 = state_32947__$1;
(statearr_32954_33000[(1)] = (5));

} else {
var statearr_32955_33001 = state_32947__$1;
(statearr_32955_33001[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (15))){
var inst_32920 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
var statearr_32956_33002 = state_32947__$1;
(statearr_32956_33002[(2)] = inst_32920);

(statearr_32956_33002[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (21))){
var inst_32940 = (state_32947[(2)]);
var state_32947__$1 = (function (){var statearr_32957 = state_32947;
(statearr_32957[(9)] = inst_32940);

return statearr_32957;
})();
var statearr_32958_33003 = state_32947__$1;
(statearr_32958_33003[(2)] = null);

(statearr_32958_33003[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (13))){
var inst_32902 = (state_32947[(10)]);
var inst_32904 = cljs.core.chunked_seq_QMARK_.call(null,inst_32902);
var state_32947__$1 = state_32947;
if(inst_32904){
var statearr_32959_33004 = state_32947__$1;
(statearr_32959_33004[(1)] = (16));

} else {
var statearr_32960_33005 = state_32947__$1;
(statearr_32960_33005[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (22))){
var inst_32932 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
if(cljs.core.truth_(inst_32932)){
var statearr_32961_33006 = state_32947__$1;
(statearr_32961_33006[(1)] = (23));

} else {
var statearr_32962_33007 = state_32947__$1;
(statearr_32962_33007[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (6))){
var inst_32878 = (state_32947[(8)]);
var inst_32928 = (state_32947[(11)]);
var inst_32926 = (state_32947[(7)]);
var inst_32926__$1 = topic_fn.call(null,inst_32878);
var inst_32927 = cljs.core.deref.call(null,mults);
var inst_32928__$1 = cljs.core.get.call(null,inst_32927,inst_32926__$1);
var state_32947__$1 = (function (){var statearr_32963 = state_32947;
(statearr_32963[(11)] = inst_32928__$1);

(statearr_32963[(7)] = inst_32926__$1);

return statearr_32963;
})();
if(cljs.core.truth_(inst_32928__$1)){
var statearr_32964_33008 = state_32947__$1;
(statearr_32964_33008[(1)] = (19));

} else {
var statearr_32965_33009 = state_32947__$1;
(statearr_32965_33009[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (25))){
var inst_32937 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
var statearr_32966_33010 = state_32947__$1;
(statearr_32966_33010[(2)] = inst_32937);

(statearr_32966_33010[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (17))){
var inst_32902 = (state_32947[(10)]);
var inst_32911 = cljs.core.first.call(null,inst_32902);
var inst_32912 = cljs.core.async.muxch_STAR_.call(null,inst_32911);
var inst_32913 = cljs.core.async.close_BANG_.call(null,inst_32912);
var inst_32914 = cljs.core.next.call(null,inst_32902);
var inst_32888 = inst_32914;
var inst_32889 = null;
var inst_32890 = (0);
var inst_32891 = (0);
var state_32947__$1 = (function (){var statearr_32967 = state_32947;
(statearr_32967[(12)] = inst_32890);

(statearr_32967[(13)] = inst_32913);

(statearr_32967[(14)] = inst_32888);

(statearr_32967[(15)] = inst_32889);

(statearr_32967[(16)] = inst_32891);

return statearr_32967;
})();
var statearr_32968_33011 = state_32947__$1;
(statearr_32968_33011[(2)] = null);

(statearr_32968_33011[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (3))){
var inst_32945 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32947__$1,inst_32945);
} else {
if((state_val_32948 === (12))){
var inst_32922 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
var statearr_32969_33012 = state_32947__$1;
(statearr_32969_33012[(2)] = inst_32922);

(statearr_32969_33012[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (2))){
var state_32947__$1 = state_32947;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32947__$1,(4),ch);
} else {
if((state_val_32948 === (23))){
var state_32947__$1 = state_32947;
var statearr_32970_33013 = state_32947__$1;
(statearr_32970_33013[(2)] = null);

(statearr_32970_33013[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (19))){
var inst_32878 = (state_32947[(8)]);
var inst_32928 = (state_32947[(11)]);
var inst_32930 = cljs.core.async.muxch_STAR_.call(null,inst_32928);
var state_32947__$1 = state_32947;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32947__$1,(22),inst_32930,inst_32878);
} else {
if((state_val_32948 === (11))){
var inst_32902 = (state_32947[(10)]);
var inst_32888 = (state_32947[(14)]);
var inst_32902__$1 = cljs.core.seq.call(null,inst_32888);
var state_32947__$1 = (function (){var statearr_32971 = state_32947;
(statearr_32971[(10)] = inst_32902__$1);

return statearr_32971;
})();
if(inst_32902__$1){
var statearr_32972_33014 = state_32947__$1;
(statearr_32972_33014[(1)] = (13));

} else {
var statearr_32973_33015 = state_32947__$1;
(statearr_32973_33015[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (9))){
var inst_32924 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
var statearr_32974_33016 = state_32947__$1;
(statearr_32974_33016[(2)] = inst_32924);

(statearr_32974_33016[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (5))){
var inst_32885 = cljs.core.deref.call(null,mults);
var inst_32886 = cljs.core.vals.call(null,inst_32885);
var inst_32887 = cljs.core.seq.call(null,inst_32886);
var inst_32888 = inst_32887;
var inst_32889 = null;
var inst_32890 = (0);
var inst_32891 = (0);
var state_32947__$1 = (function (){var statearr_32975 = state_32947;
(statearr_32975[(12)] = inst_32890);

(statearr_32975[(14)] = inst_32888);

(statearr_32975[(15)] = inst_32889);

(statearr_32975[(16)] = inst_32891);

return statearr_32975;
})();
var statearr_32976_33017 = state_32947__$1;
(statearr_32976_33017[(2)] = null);

(statearr_32976_33017[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (14))){
var state_32947__$1 = state_32947;
var statearr_32980_33018 = state_32947__$1;
(statearr_32980_33018[(2)] = null);

(statearr_32980_33018[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (16))){
var inst_32902 = (state_32947[(10)]);
var inst_32906 = cljs.core.chunk_first.call(null,inst_32902);
var inst_32907 = cljs.core.chunk_rest.call(null,inst_32902);
var inst_32908 = cljs.core.count.call(null,inst_32906);
var inst_32888 = inst_32907;
var inst_32889 = inst_32906;
var inst_32890 = inst_32908;
var inst_32891 = (0);
var state_32947__$1 = (function (){var statearr_32981 = state_32947;
(statearr_32981[(12)] = inst_32890);

(statearr_32981[(14)] = inst_32888);

(statearr_32981[(15)] = inst_32889);

(statearr_32981[(16)] = inst_32891);

return statearr_32981;
})();
var statearr_32982_33019 = state_32947__$1;
(statearr_32982_33019[(2)] = null);

(statearr_32982_33019[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (10))){
var inst_32890 = (state_32947[(12)]);
var inst_32888 = (state_32947[(14)]);
var inst_32889 = (state_32947[(15)]);
var inst_32891 = (state_32947[(16)]);
var inst_32896 = cljs.core._nth.call(null,inst_32889,inst_32891);
var inst_32897 = cljs.core.async.muxch_STAR_.call(null,inst_32896);
var inst_32898 = cljs.core.async.close_BANG_.call(null,inst_32897);
var inst_32899 = (inst_32891 + (1));
var tmp32977 = inst_32890;
var tmp32978 = inst_32888;
var tmp32979 = inst_32889;
var inst_32888__$1 = tmp32978;
var inst_32889__$1 = tmp32979;
var inst_32890__$1 = tmp32977;
var inst_32891__$1 = inst_32899;
var state_32947__$1 = (function (){var statearr_32983 = state_32947;
(statearr_32983[(12)] = inst_32890__$1);

(statearr_32983[(14)] = inst_32888__$1);

(statearr_32983[(15)] = inst_32889__$1);

(statearr_32983[(17)] = inst_32898);

(statearr_32983[(16)] = inst_32891__$1);

return statearr_32983;
})();
var statearr_32984_33020 = state_32947__$1;
(statearr_32984_33020[(2)] = null);

(statearr_32984_33020[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (18))){
var inst_32917 = (state_32947[(2)]);
var state_32947__$1 = state_32947;
var statearr_32985_33021 = state_32947__$1;
(statearr_32985_33021[(2)] = inst_32917);

(statearr_32985_33021[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32948 === (8))){
var inst_32890 = (state_32947[(12)]);
var inst_32891 = (state_32947[(16)]);
var inst_32893 = (inst_32891 < inst_32890);
var inst_32894 = inst_32893;
var state_32947__$1 = state_32947;
if(cljs.core.truth_(inst_32894)){
var statearr_32986_33022 = state_32947__$1;
(statearr_32986_33022[(1)] = (10));

} else {
var statearr_32987_33023 = state_32947__$1;
(statearr_32987_33023[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___32995,mults,ensure_mult,p))
;
return ((function (switch__7585__auto__,c__7600__auto___32995,mults,ensure_mult,p){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_32991 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32991[(0)] = state_machine__7586__auto__);

(statearr_32991[(1)] = (1));

return statearr_32991;
});
var state_machine__7586__auto____1 = (function (state_32947){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_32947);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e32992){if((e32992 instanceof Object)){
var ex__7589__auto__ = e32992;
var statearr_32993_33024 = state_32947;
(statearr_32993_33024[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32947);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32992;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33025 = state_32947;
state_32947 = G__33025;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_32947){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_32947);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___32995,mults,ensure_mult,p))
})();
var state__7602__auto__ = (function (){var statearr_32994 = f__7601__auto__.call(null);
(statearr_32994[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___32995);

return statearr_32994;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___32995,mults,ensure_mult,p))
);


return p;
});
pub = function(ch,topic_fn,buf_fn){
switch(arguments.length){
case 2:
return pub__2.call(this,ch,topic_fn);
case 3:
return pub__3.call(this,ch,topic_fn,buf_fn);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pub.cljs$core$IFn$_invoke$arity$2 = pub__2;
pub.cljs$core$IFn$_invoke$arity$3 = pub__3;
return pub;
})()
;
/**
* Subscribes a channel to a topic of a pub.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.sub = (function() {
var sub = null;
var sub__3 = (function (p,topic,ch){
return sub.call(null,p,topic,ch,true);
});
var sub__4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});
sub = function(p,topic,ch,close_QMARK_){
switch(arguments.length){
case 3:
return sub__3.call(this,p,topic,ch);
case 4:
return sub__4.call(this,p,topic,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
sub.cljs$core$IFn$_invoke$arity$3 = sub__3;
sub.cljs$core$IFn$_invoke$arity$4 = sub__4;
return sub;
})()
;
/**
* Unsubscribes a channel from a topic of a pub
*/
cljs.core.async.unsub = (function unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
* Unsubscribes all channels from a pub, or a topic of a pub
*/
cljs.core.async.unsub_all = (function() {
var unsub_all = null;
var unsub_all__1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});
var unsub_all__2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});
unsub_all = function(p,topic){
switch(arguments.length){
case 1:
return unsub_all__1.call(this,p);
case 2:
return unsub_all__2.call(this,p,topic);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all.cljs$core$IFn$_invoke$arity$1 = unsub_all__1;
unsub_all.cljs$core$IFn$_invoke$arity$2 = unsub_all__2;
return unsub_all;
})()
;
/**
* Takes a function and a collection of source channels, and returns a
* channel which contains the values produced by applying f to the set
* of first items taken from each source channel, followed by applying
* f to the set of second items from each channel, until any one of the
* channels is closed, at which point the output channel will be
* closed. The returned channel will be unbuffered by default, or a
* buf-or-n can be supplied
*/
cljs.core.async.map = (function() {
var map = null;
var map__2 = (function (f,chs){
return map.call(null,f,chs,null);
});
var map__3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__7600__auto___33162 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___33162,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___33162,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_33132){
var state_val_33133 = (state_33132[(1)]);
if((state_val_33133 === (7))){
var state_33132__$1 = state_33132;
var statearr_33134_33163 = state_33132__$1;
(statearr_33134_33163[(2)] = null);

(statearr_33134_33163[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (1))){
var state_33132__$1 = state_33132;
var statearr_33135_33164 = state_33132__$1;
(statearr_33135_33164[(2)] = null);

(statearr_33135_33164[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (4))){
var inst_33096 = (state_33132[(7)]);
var inst_33098 = (inst_33096 < cnt);
var state_33132__$1 = state_33132;
if(cljs.core.truth_(inst_33098)){
var statearr_33136_33165 = state_33132__$1;
(statearr_33136_33165[(1)] = (6));

} else {
var statearr_33137_33166 = state_33132__$1;
(statearr_33137_33166[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (15))){
var inst_33128 = (state_33132[(2)]);
var state_33132__$1 = state_33132;
var statearr_33138_33167 = state_33132__$1;
(statearr_33138_33167[(2)] = inst_33128);

(statearr_33138_33167[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (13))){
var inst_33121 = cljs.core.async.close_BANG_.call(null,out);
var state_33132__$1 = state_33132;
var statearr_33139_33168 = state_33132__$1;
(statearr_33139_33168[(2)] = inst_33121);

(statearr_33139_33168[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (6))){
var state_33132__$1 = state_33132;
var statearr_33140_33169 = state_33132__$1;
(statearr_33140_33169[(2)] = null);

(statearr_33140_33169[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (3))){
var inst_33130 = (state_33132[(2)]);
var state_33132__$1 = state_33132;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33132__$1,inst_33130);
} else {
if((state_val_33133 === (12))){
var inst_33118 = (state_33132[(8)]);
var inst_33118__$1 = (state_33132[(2)]);
var inst_33119 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_33118__$1);
var state_33132__$1 = (function (){var statearr_33141 = state_33132;
(statearr_33141[(8)] = inst_33118__$1);

return statearr_33141;
})();
if(cljs.core.truth_(inst_33119)){
var statearr_33142_33170 = state_33132__$1;
(statearr_33142_33170[(1)] = (13));

} else {
var statearr_33143_33171 = state_33132__$1;
(statearr_33143_33171[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (2))){
var inst_33095 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_33096 = (0);
var state_33132__$1 = (function (){var statearr_33144 = state_33132;
(statearr_33144[(9)] = inst_33095);

(statearr_33144[(7)] = inst_33096);

return statearr_33144;
})();
var statearr_33145_33172 = state_33132__$1;
(statearr_33145_33172[(2)] = null);

(statearr_33145_33172[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (11))){
var inst_33096 = (state_33132[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_33132,(10),Object,null,(9));
var inst_33105 = chs__$1.call(null,inst_33096);
var inst_33106 = done.call(null,inst_33096);
var inst_33107 = cljs.core.async.take_BANG_.call(null,inst_33105,inst_33106);
var state_33132__$1 = state_33132;
var statearr_33146_33173 = state_33132__$1;
(statearr_33146_33173[(2)] = inst_33107);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33132__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (9))){
var inst_33096 = (state_33132[(7)]);
var inst_33109 = (state_33132[(2)]);
var inst_33110 = (inst_33096 + (1));
var inst_33096__$1 = inst_33110;
var state_33132__$1 = (function (){var statearr_33147 = state_33132;
(statearr_33147[(10)] = inst_33109);

(statearr_33147[(7)] = inst_33096__$1);

return statearr_33147;
})();
var statearr_33148_33174 = state_33132__$1;
(statearr_33148_33174[(2)] = null);

(statearr_33148_33174[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (5))){
var inst_33116 = (state_33132[(2)]);
var state_33132__$1 = (function (){var statearr_33149 = state_33132;
(statearr_33149[(11)] = inst_33116);

return statearr_33149;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33132__$1,(12),dchan);
} else {
if((state_val_33133 === (14))){
var inst_33118 = (state_33132[(8)]);
var inst_33123 = cljs.core.apply.call(null,f,inst_33118);
var state_33132__$1 = state_33132;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33132__$1,(16),out,inst_33123);
} else {
if((state_val_33133 === (16))){
var inst_33125 = (state_33132[(2)]);
var state_33132__$1 = (function (){var statearr_33150 = state_33132;
(statearr_33150[(12)] = inst_33125);

return statearr_33150;
})();
var statearr_33151_33175 = state_33132__$1;
(statearr_33151_33175[(2)] = null);

(statearr_33151_33175[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (10))){
var inst_33100 = (state_33132[(2)]);
var inst_33101 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_33132__$1 = (function (){var statearr_33152 = state_33132;
(statearr_33152[(13)] = inst_33100);

return statearr_33152;
})();
var statearr_33153_33176 = state_33132__$1;
(statearr_33153_33176[(2)] = inst_33101);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33132__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33133 === (8))){
var inst_33114 = (state_33132[(2)]);
var state_33132__$1 = state_33132;
var statearr_33154_33177 = state_33132__$1;
(statearr_33154_33177[(2)] = inst_33114);

(statearr_33154_33177[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___33162,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__7585__auto__,c__7600__auto___33162,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33158 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33158[(0)] = state_machine__7586__auto__);

(statearr_33158[(1)] = (1));

return statearr_33158;
});
var state_machine__7586__auto____1 = (function (state_33132){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33132);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33159){if((e33159 instanceof Object)){
var ex__7589__auto__ = e33159;
var statearr_33160_33178 = state_33132;
(statearr_33160_33178[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33132);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33159;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33179 = state_33132;
state_33132 = G__33179;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33132){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33132);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___33162,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__7602__auto__ = (function (){var statearr_33161 = f__7601__auto__.call(null);
(statearr_33161[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___33162);

return statearr_33161;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___33162,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});
map = function(f,chs,buf_or_n){
switch(arguments.length){
case 2:
return map__2.call(this,f,chs);
case 3:
return map__3.call(this,f,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
map.cljs$core$IFn$_invoke$arity$2 = map__2;
map.cljs$core$IFn$_invoke$arity$3 = map__3;
return map;
})()
;
/**
* Takes a collection of source channels and returns a channel which
* contains all values taken from them. The returned channel will be
* unbuffered by default, or a buf-or-n can be supplied. The channel
* will close after all the source channels have closed.
*/
cljs.core.async.merge = (function() {
var merge = null;
var merge__1 = (function (chs){
return merge.call(null,chs,null);
});
var merge__2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__7600__auto___33287 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___33287,out){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___33287,out){
return (function (state_33263){
var state_val_33264 = (state_33263[(1)]);
if((state_val_33264 === (7))){
var inst_33243 = (state_33263[(7)]);
var inst_33242 = (state_33263[(8)]);
var inst_33242__$1 = (state_33263[(2)]);
var inst_33243__$1 = cljs.core.nth.call(null,inst_33242__$1,(0),null);
var inst_33244 = cljs.core.nth.call(null,inst_33242__$1,(1),null);
var inst_33245 = (inst_33243__$1 == null);
var state_33263__$1 = (function (){var statearr_33265 = state_33263;
(statearr_33265[(7)] = inst_33243__$1);

(statearr_33265[(9)] = inst_33244);

(statearr_33265[(8)] = inst_33242__$1);

return statearr_33265;
})();
if(cljs.core.truth_(inst_33245)){
var statearr_33266_33288 = state_33263__$1;
(statearr_33266_33288[(1)] = (8));

} else {
var statearr_33267_33289 = state_33263__$1;
(statearr_33267_33289[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (1))){
var inst_33234 = cljs.core.vec.call(null,chs);
var inst_33235 = inst_33234;
var state_33263__$1 = (function (){var statearr_33268 = state_33263;
(statearr_33268[(10)] = inst_33235);

return statearr_33268;
})();
var statearr_33269_33290 = state_33263__$1;
(statearr_33269_33290[(2)] = null);

(statearr_33269_33290[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (4))){
var inst_33235 = (state_33263[(10)]);
var state_33263__$1 = state_33263;
return cljs.core.async.ioc_alts_BANG_.call(null,state_33263__$1,(7),inst_33235);
} else {
if((state_val_33264 === (6))){
var inst_33259 = (state_33263[(2)]);
var state_33263__$1 = state_33263;
var statearr_33270_33291 = state_33263__$1;
(statearr_33270_33291[(2)] = inst_33259);

(statearr_33270_33291[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (3))){
var inst_33261 = (state_33263[(2)]);
var state_33263__$1 = state_33263;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33263__$1,inst_33261);
} else {
if((state_val_33264 === (2))){
var inst_33235 = (state_33263[(10)]);
var inst_33237 = cljs.core.count.call(null,inst_33235);
var inst_33238 = (inst_33237 > (0));
var state_33263__$1 = state_33263;
if(cljs.core.truth_(inst_33238)){
var statearr_33272_33292 = state_33263__$1;
(statearr_33272_33292[(1)] = (4));

} else {
var statearr_33273_33293 = state_33263__$1;
(statearr_33273_33293[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (11))){
var inst_33235 = (state_33263[(10)]);
var inst_33252 = (state_33263[(2)]);
var tmp33271 = inst_33235;
var inst_33235__$1 = tmp33271;
var state_33263__$1 = (function (){var statearr_33274 = state_33263;
(statearr_33274[(10)] = inst_33235__$1);

(statearr_33274[(11)] = inst_33252);

return statearr_33274;
})();
var statearr_33275_33294 = state_33263__$1;
(statearr_33275_33294[(2)] = null);

(statearr_33275_33294[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (9))){
var inst_33243 = (state_33263[(7)]);
var state_33263__$1 = state_33263;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33263__$1,(11),out,inst_33243);
} else {
if((state_val_33264 === (5))){
var inst_33257 = cljs.core.async.close_BANG_.call(null,out);
var state_33263__$1 = state_33263;
var statearr_33276_33295 = state_33263__$1;
(statearr_33276_33295[(2)] = inst_33257);

(statearr_33276_33295[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (10))){
var inst_33255 = (state_33263[(2)]);
var state_33263__$1 = state_33263;
var statearr_33277_33296 = state_33263__$1;
(statearr_33277_33296[(2)] = inst_33255);

(statearr_33277_33296[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33264 === (8))){
var inst_33243 = (state_33263[(7)]);
var inst_33235 = (state_33263[(10)]);
var inst_33244 = (state_33263[(9)]);
var inst_33242 = (state_33263[(8)]);
var inst_33247 = (function (){var c = inst_33244;
var v = inst_33243;
var vec__33240 = inst_33242;
var cs = inst_33235;
return ((function (c,v,vec__33240,cs,inst_33243,inst_33235,inst_33244,inst_33242,state_val_33264,c__7600__auto___33287,out){
return (function (p1__33180_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__33180_SHARP_);
});
;})(c,v,vec__33240,cs,inst_33243,inst_33235,inst_33244,inst_33242,state_val_33264,c__7600__auto___33287,out))
})();
var inst_33248 = cljs.core.filterv.call(null,inst_33247,inst_33235);
var inst_33235__$1 = inst_33248;
var state_33263__$1 = (function (){var statearr_33278 = state_33263;
(statearr_33278[(10)] = inst_33235__$1);

return statearr_33278;
})();
var statearr_33279_33297 = state_33263__$1;
(statearr_33279_33297[(2)] = null);

(statearr_33279_33297[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___33287,out))
;
return ((function (switch__7585__auto__,c__7600__auto___33287,out){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33283 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33283[(0)] = state_machine__7586__auto__);

(statearr_33283[(1)] = (1));

return statearr_33283;
});
var state_machine__7586__auto____1 = (function (state_33263){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33263);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33284){if((e33284 instanceof Object)){
var ex__7589__auto__ = e33284;
var statearr_33285_33298 = state_33263;
(statearr_33285_33298[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33263);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33284;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33299 = state_33263;
state_33263 = G__33299;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33263){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33263);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___33287,out))
})();
var state__7602__auto__ = (function (){var statearr_33286 = f__7601__auto__.call(null);
(statearr_33286[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___33287);

return statearr_33286;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___33287,out))
);


return out;
});
merge = function(chs,buf_or_n){
switch(arguments.length){
case 1:
return merge__1.call(this,chs);
case 2:
return merge__2.call(this,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
merge.cljs$core$IFn$_invoke$arity$1 = merge__1;
merge.cljs$core$IFn$_invoke$arity$2 = merge__2;
return merge;
})()
;
/**
* Returns a channel containing the single (collection) result of the
* items taken from the channel conjoined to the supplied
* collection. ch must close before into produces a result.
*/
cljs.core.async.into = (function into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
* Returns a channel that will return, at most, n items from ch. After n items
* have been returned, or ch has been closed, the return chanel will close.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.take = (function() {
var take = null;
var take__2 = (function (n,ch){
return take.call(null,n,ch,null);
});
var take__3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__7600__auto___33392 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___33392,out){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___33392,out){
return (function (state_33369){
var state_val_33370 = (state_33369[(1)]);
if((state_val_33370 === (7))){
var inst_33351 = (state_33369[(7)]);
var inst_33351__$1 = (state_33369[(2)]);
var inst_33352 = (inst_33351__$1 == null);
var inst_33353 = cljs.core.not.call(null,inst_33352);
var state_33369__$1 = (function (){var statearr_33371 = state_33369;
(statearr_33371[(7)] = inst_33351__$1);

return statearr_33371;
})();
if(inst_33353){
var statearr_33372_33393 = state_33369__$1;
(statearr_33372_33393[(1)] = (8));

} else {
var statearr_33373_33394 = state_33369__$1;
(statearr_33373_33394[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (1))){
var inst_33346 = (0);
var state_33369__$1 = (function (){var statearr_33374 = state_33369;
(statearr_33374[(8)] = inst_33346);

return statearr_33374;
})();
var statearr_33375_33395 = state_33369__$1;
(statearr_33375_33395[(2)] = null);

(statearr_33375_33395[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (4))){
var state_33369__$1 = state_33369;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33369__$1,(7),ch);
} else {
if((state_val_33370 === (6))){
var inst_33364 = (state_33369[(2)]);
var state_33369__$1 = state_33369;
var statearr_33376_33396 = state_33369__$1;
(statearr_33376_33396[(2)] = inst_33364);

(statearr_33376_33396[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (3))){
var inst_33366 = (state_33369[(2)]);
var inst_33367 = cljs.core.async.close_BANG_.call(null,out);
var state_33369__$1 = (function (){var statearr_33377 = state_33369;
(statearr_33377[(9)] = inst_33366);

return statearr_33377;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33369__$1,inst_33367);
} else {
if((state_val_33370 === (2))){
var inst_33346 = (state_33369[(8)]);
var inst_33348 = (inst_33346 < n);
var state_33369__$1 = state_33369;
if(cljs.core.truth_(inst_33348)){
var statearr_33378_33397 = state_33369__$1;
(statearr_33378_33397[(1)] = (4));

} else {
var statearr_33379_33398 = state_33369__$1;
(statearr_33379_33398[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (11))){
var inst_33346 = (state_33369[(8)]);
var inst_33356 = (state_33369[(2)]);
var inst_33357 = (inst_33346 + (1));
var inst_33346__$1 = inst_33357;
var state_33369__$1 = (function (){var statearr_33380 = state_33369;
(statearr_33380[(10)] = inst_33356);

(statearr_33380[(8)] = inst_33346__$1);

return statearr_33380;
})();
var statearr_33381_33399 = state_33369__$1;
(statearr_33381_33399[(2)] = null);

(statearr_33381_33399[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (9))){
var state_33369__$1 = state_33369;
var statearr_33382_33400 = state_33369__$1;
(statearr_33382_33400[(2)] = null);

(statearr_33382_33400[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (5))){
var state_33369__$1 = state_33369;
var statearr_33383_33401 = state_33369__$1;
(statearr_33383_33401[(2)] = null);

(statearr_33383_33401[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (10))){
var inst_33361 = (state_33369[(2)]);
var state_33369__$1 = state_33369;
var statearr_33384_33402 = state_33369__$1;
(statearr_33384_33402[(2)] = inst_33361);

(statearr_33384_33402[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33370 === (8))){
var inst_33351 = (state_33369[(7)]);
var state_33369__$1 = state_33369;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33369__$1,(11),out,inst_33351);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___33392,out))
;
return ((function (switch__7585__auto__,c__7600__auto___33392,out){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33388 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33388[(0)] = state_machine__7586__auto__);

(statearr_33388[(1)] = (1));

return statearr_33388;
});
var state_machine__7586__auto____1 = (function (state_33369){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33369);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33389){if((e33389 instanceof Object)){
var ex__7589__auto__ = e33389;
var statearr_33390_33403 = state_33369;
(statearr_33390_33403[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33369);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33389;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33404 = state_33369;
state_33369 = G__33404;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33369){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33369);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___33392,out))
})();
var state__7602__auto__ = (function (){var statearr_33391 = f__7601__auto__.call(null);
(statearr_33391[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___33392);

return statearr_33391;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___33392,out))
);


return out;
});
take = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return take__2.call(this,n,ch);
case 3:
return take__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take.cljs$core$IFn$_invoke$arity$2 = take__2;
take.cljs$core$IFn$_invoke$arity$3 = take__3;
return take;
})()
;
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.map_LT_ = (function map_LT_(f,ch){
if(typeof cljs.core.async.t33412 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t33412 = (function (ch,f,map_LT_,meta33413){
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta33413 = meta33413;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t33415 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t33415 = (function (fn1,_,meta33413,map_LT_,f,ch,meta33416){
this.fn1 = fn1;
this._ = _;
this.meta33413 = meta33413;
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta33416 = meta33416;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t33415.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t33415.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t33415.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__33405_SHARP_){
return f1.call(null,(((p1__33405_SHARP_ == null))?null:self__.f.call(null,p1__33405_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t33415.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_33417){
var self__ = this;
var _33417__$1 = this;
return self__.meta33416;
});})(___$1))
;

cljs.core.async.t33415.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_33417,meta33416__$1){
var self__ = this;
var _33417__$1 = this;
return (new cljs.core.async.t33415(self__.fn1,self__._,self__.meta33413,self__.map_LT_,self__.f,self__.ch,meta33416__$1));
});})(___$1))
;

cljs.core.async.t33415.cljs$lang$type = true;

cljs.core.async.t33415.cljs$lang$ctorStr = "cljs.core.async/t33415";

cljs.core.async.t33415.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t33415");
});})(___$1))
;

cljs.core.async.__GT_t33415 = ((function (___$1){
return (function __GT_t33415(fn1__$1,___$2,meta33413__$1,map_LT___$1,f__$1,ch__$1,meta33416){
return (new cljs.core.async.t33415(fn1__$1,___$2,meta33413__$1,map_LT___$1,f__$1,ch__$1,meta33416));
});})(___$1))
;

}

return (new cljs.core.async.t33415(fn1,___$1,self__.meta33413,self__.map_LT_,self__.f,self__.ch,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__3969__auto__ = ret;
if(cljs.core.truth_(and__3969__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__3969__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t33412.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t33412.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33414){
var self__ = this;
var _33414__$1 = this;
return self__.meta33413;
});

cljs.core.async.t33412.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33414,meta33413__$1){
var self__ = this;
var _33414__$1 = this;
return (new cljs.core.async.t33412(self__.ch,self__.f,self__.map_LT_,meta33413__$1));
});

cljs.core.async.t33412.cljs$lang$type = true;

cljs.core.async.t33412.cljs$lang$ctorStr = "cljs.core.async/t33412";

cljs.core.async.t33412.cljs$lang$ctorPrWriter = (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t33412");
});

cljs.core.async.__GT_t33412 = (function __GT_t33412(ch__$1,f__$1,map_LT___$1,meta33413){
return (new cljs.core.async.t33412(ch__$1,f__$1,map_LT___$1,meta33413));
});

}

return (new cljs.core.async.t33412(ch,f,map_LT_,cljs.core.PersistentArrayMap.EMPTY));
});
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.map_GT_ = (function map_GT_(f,ch){
if(typeof cljs.core.async.t33421 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t33421 = (function (ch,f,map_GT_,meta33422){
this.ch = ch;
this.f = f;
this.map_GT_ = map_GT_;
this.meta33422 = meta33422;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t33421.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t33421.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t33421.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t33421.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t33421.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t33421.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t33421.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33423){
var self__ = this;
var _33423__$1 = this;
return self__.meta33422;
});

cljs.core.async.t33421.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33423,meta33422__$1){
var self__ = this;
var _33423__$1 = this;
return (new cljs.core.async.t33421(self__.ch,self__.f,self__.map_GT_,meta33422__$1));
});

cljs.core.async.t33421.cljs$lang$type = true;

cljs.core.async.t33421.cljs$lang$ctorStr = "cljs.core.async/t33421";

cljs.core.async.t33421.cljs$lang$ctorPrWriter = (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t33421");
});

cljs.core.async.__GT_t33421 = (function __GT_t33421(ch__$1,f__$1,map_GT___$1,meta33422){
return (new cljs.core.async.t33421(ch__$1,f__$1,map_GT___$1,meta33422));
});

}

return (new cljs.core.async.t33421(ch,f,map_GT_,cljs.core.PersistentArrayMap.EMPTY));
});
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.filter_GT_ = (function filter_GT_(p,ch){
if(typeof cljs.core.async.t33427 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t33427 = (function (ch,p,filter_GT_,meta33428){
this.ch = ch;
this.p = p;
this.filter_GT_ = filter_GT_;
this.meta33428 = meta33428;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t33427.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t33427.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33429){
var self__ = this;
var _33429__$1 = this;
return self__.meta33428;
});

cljs.core.async.t33427.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33429,meta33428__$1){
var self__ = this;
var _33429__$1 = this;
return (new cljs.core.async.t33427(self__.ch,self__.p,self__.filter_GT_,meta33428__$1));
});

cljs.core.async.t33427.cljs$lang$type = true;

cljs.core.async.t33427.cljs$lang$ctorStr = "cljs.core.async/t33427";

cljs.core.async.t33427.cljs$lang$ctorPrWriter = (function (this__4568__auto__,writer__4569__auto__,opt__4570__auto__){
return cljs.core._write.call(null,writer__4569__auto__,"cljs.core.async/t33427");
});

cljs.core.async.__GT_t33427 = (function __GT_t33427(ch__$1,p__$1,filter_GT___$1,meta33428){
return (new cljs.core.async.t33427(ch__$1,p__$1,filter_GT___$1,meta33428));
});

}

return (new cljs.core.async.t33427(ch,p,filter_GT_,cljs.core.PersistentArrayMap.EMPTY));
});
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.remove_GT_ = (function remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.filter_LT_ = (function() {
var filter_LT_ = null;
var filter_LT___2 = (function (p,ch){
return filter_LT_.call(null,p,ch,null);
});
var filter_LT___3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__7600__auto___33512 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___33512,out){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___33512,out){
return (function (state_33491){
var state_val_33492 = (state_33491[(1)]);
if((state_val_33492 === (7))){
var inst_33487 = (state_33491[(2)]);
var state_33491__$1 = state_33491;
var statearr_33493_33513 = state_33491__$1;
(statearr_33493_33513[(2)] = inst_33487);

(statearr_33493_33513[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (1))){
var state_33491__$1 = state_33491;
var statearr_33494_33514 = state_33491__$1;
(statearr_33494_33514[(2)] = null);

(statearr_33494_33514[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (4))){
var inst_33473 = (state_33491[(7)]);
var inst_33473__$1 = (state_33491[(2)]);
var inst_33474 = (inst_33473__$1 == null);
var state_33491__$1 = (function (){var statearr_33495 = state_33491;
(statearr_33495[(7)] = inst_33473__$1);

return statearr_33495;
})();
if(cljs.core.truth_(inst_33474)){
var statearr_33496_33515 = state_33491__$1;
(statearr_33496_33515[(1)] = (5));

} else {
var statearr_33497_33516 = state_33491__$1;
(statearr_33497_33516[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (6))){
var inst_33473 = (state_33491[(7)]);
var inst_33478 = p.call(null,inst_33473);
var state_33491__$1 = state_33491;
if(cljs.core.truth_(inst_33478)){
var statearr_33498_33517 = state_33491__$1;
(statearr_33498_33517[(1)] = (8));

} else {
var statearr_33499_33518 = state_33491__$1;
(statearr_33499_33518[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (3))){
var inst_33489 = (state_33491[(2)]);
var state_33491__$1 = state_33491;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33491__$1,inst_33489);
} else {
if((state_val_33492 === (2))){
var state_33491__$1 = state_33491;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33491__$1,(4),ch);
} else {
if((state_val_33492 === (11))){
var inst_33481 = (state_33491[(2)]);
var state_33491__$1 = state_33491;
var statearr_33500_33519 = state_33491__$1;
(statearr_33500_33519[(2)] = inst_33481);

(statearr_33500_33519[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (9))){
var state_33491__$1 = state_33491;
var statearr_33501_33520 = state_33491__$1;
(statearr_33501_33520[(2)] = null);

(statearr_33501_33520[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (5))){
var inst_33476 = cljs.core.async.close_BANG_.call(null,out);
var state_33491__$1 = state_33491;
var statearr_33502_33521 = state_33491__$1;
(statearr_33502_33521[(2)] = inst_33476);

(statearr_33502_33521[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (10))){
var inst_33484 = (state_33491[(2)]);
var state_33491__$1 = (function (){var statearr_33503 = state_33491;
(statearr_33503[(8)] = inst_33484);

return statearr_33503;
})();
var statearr_33504_33522 = state_33491__$1;
(statearr_33504_33522[(2)] = null);

(statearr_33504_33522[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33492 === (8))){
var inst_33473 = (state_33491[(7)]);
var state_33491__$1 = state_33491;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33491__$1,(11),out,inst_33473);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___33512,out))
;
return ((function (switch__7585__auto__,c__7600__auto___33512,out){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33508 = [null,null,null,null,null,null,null,null,null];
(statearr_33508[(0)] = state_machine__7586__auto__);

(statearr_33508[(1)] = (1));

return statearr_33508;
});
var state_machine__7586__auto____1 = (function (state_33491){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33491);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33509){if((e33509 instanceof Object)){
var ex__7589__auto__ = e33509;
var statearr_33510_33523 = state_33491;
(statearr_33510_33523[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33491);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33509;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33524 = state_33491;
state_33491 = G__33524;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33491){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33491);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___33512,out))
})();
var state__7602__auto__ = (function (){var statearr_33511 = f__7601__auto__.call(null);
(statearr_33511[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___33512);

return statearr_33511;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___33512,out))
);


return out;
});
filter_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return filter_LT___2.call(this,p,ch);
case 3:
return filter_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
filter_LT_.cljs$core$IFn$_invoke$arity$2 = filter_LT___2;
filter_LT_.cljs$core$IFn$_invoke$arity$3 = filter_LT___3;
return filter_LT_;
})()
;
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.remove_LT_ = (function() {
var remove_LT_ = null;
var remove_LT___2 = (function (p,ch){
return remove_LT_.call(null,p,ch,null);
});
var remove_LT___3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});
remove_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return remove_LT___2.call(this,p,ch);
case 3:
return remove_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
remove_LT_.cljs$core$IFn$_invoke$arity$2 = remove_LT___2;
remove_LT_.cljs$core$IFn$_invoke$arity$3 = remove_LT___3;
return remove_LT_;
})()
;
cljs.core.async.mapcat_STAR_ = (function mapcat_STAR_(f,in$,out){
var c__7600__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto__){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto__){
return (function (state_33690){
var state_val_33691 = (state_33690[(1)]);
if((state_val_33691 === (7))){
var inst_33686 = (state_33690[(2)]);
var state_33690__$1 = state_33690;
var statearr_33692_33733 = state_33690__$1;
(statearr_33692_33733[(2)] = inst_33686);

(statearr_33692_33733[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (20))){
var inst_33656 = (state_33690[(7)]);
var inst_33667 = (state_33690[(2)]);
var inst_33668 = cljs.core.next.call(null,inst_33656);
var inst_33642 = inst_33668;
var inst_33643 = null;
var inst_33644 = (0);
var inst_33645 = (0);
var state_33690__$1 = (function (){var statearr_33693 = state_33690;
(statearr_33693[(8)] = inst_33667);

(statearr_33693[(9)] = inst_33643);

(statearr_33693[(10)] = inst_33645);

(statearr_33693[(11)] = inst_33644);

(statearr_33693[(12)] = inst_33642);

return statearr_33693;
})();
var statearr_33694_33734 = state_33690__$1;
(statearr_33694_33734[(2)] = null);

(statearr_33694_33734[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (1))){
var state_33690__$1 = state_33690;
var statearr_33695_33735 = state_33690__$1;
(statearr_33695_33735[(2)] = null);

(statearr_33695_33735[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (4))){
var inst_33631 = (state_33690[(13)]);
var inst_33631__$1 = (state_33690[(2)]);
var inst_33632 = (inst_33631__$1 == null);
var state_33690__$1 = (function (){var statearr_33696 = state_33690;
(statearr_33696[(13)] = inst_33631__$1);

return statearr_33696;
})();
if(cljs.core.truth_(inst_33632)){
var statearr_33697_33736 = state_33690__$1;
(statearr_33697_33736[(1)] = (5));

} else {
var statearr_33698_33737 = state_33690__$1;
(statearr_33698_33737[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (15))){
var state_33690__$1 = state_33690;
var statearr_33702_33738 = state_33690__$1;
(statearr_33702_33738[(2)] = null);

(statearr_33702_33738[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (21))){
var state_33690__$1 = state_33690;
var statearr_33703_33739 = state_33690__$1;
(statearr_33703_33739[(2)] = null);

(statearr_33703_33739[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (13))){
var inst_33643 = (state_33690[(9)]);
var inst_33645 = (state_33690[(10)]);
var inst_33644 = (state_33690[(11)]);
var inst_33642 = (state_33690[(12)]);
var inst_33652 = (state_33690[(2)]);
var inst_33653 = (inst_33645 + (1));
var tmp33699 = inst_33643;
var tmp33700 = inst_33644;
var tmp33701 = inst_33642;
var inst_33642__$1 = tmp33701;
var inst_33643__$1 = tmp33699;
var inst_33644__$1 = tmp33700;
var inst_33645__$1 = inst_33653;
var state_33690__$1 = (function (){var statearr_33704 = state_33690;
(statearr_33704[(9)] = inst_33643__$1);

(statearr_33704[(10)] = inst_33645__$1);

(statearr_33704[(14)] = inst_33652);

(statearr_33704[(11)] = inst_33644__$1);

(statearr_33704[(12)] = inst_33642__$1);

return statearr_33704;
})();
var statearr_33705_33740 = state_33690__$1;
(statearr_33705_33740[(2)] = null);

(statearr_33705_33740[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (22))){
var state_33690__$1 = state_33690;
var statearr_33706_33741 = state_33690__$1;
(statearr_33706_33741[(2)] = null);

(statearr_33706_33741[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (6))){
var inst_33631 = (state_33690[(13)]);
var inst_33640 = f.call(null,inst_33631);
var inst_33641 = cljs.core.seq.call(null,inst_33640);
var inst_33642 = inst_33641;
var inst_33643 = null;
var inst_33644 = (0);
var inst_33645 = (0);
var state_33690__$1 = (function (){var statearr_33707 = state_33690;
(statearr_33707[(9)] = inst_33643);

(statearr_33707[(10)] = inst_33645);

(statearr_33707[(11)] = inst_33644);

(statearr_33707[(12)] = inst_33642);

return statearr_33707;
})();
var statearr_33708_33742 = state_33690__$1;
(statearr_33708_33742[(2)] = null);

(statearr_33708_33742[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (17))){
var inst_33656 = (state_33690[(7)]);
var inst_33660 = cljs.core.chunk_first.call(null,inst_33656);
var inst_33661 = cljs.core.chunk_rest.call(null,inst_33656);
var inst_33662 = cljs.core.count.call(null,inst_33660);
var inst_33642 = inst_33661;
var inst_33643 = inst_33660;
var inst_33644 = inst_33662;
var inst_33645 = (0);
var state_33690__$1 = (function (){var statearr_33709 = state_33690;
(statearr_33709[(9)] = inst_33643);

(statearr_33709[(10)] = inst_33645);

(statearr_33709[(11)] = inst_33644);

(statearr_33709[(12)] = inst_33642);

return statearr_33709;
})();
var statearr_33710_33743 = state_33690__$1;
(statearr_33710_33743[(2)] = null);

(statearr_33710_33743[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (3))){
var inst_33688 = (state_33690[(2)]);
var state_33690__$1 = state_33690;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33690__$1,inst_33688);
} else {
if((state_val_33691 === (12))){
var inst_33676 = (state_33690[(2)]);
var state_33690__$1 = state_33690;
var statearr_33711_33744 = state_33690__$1;
(statearr_33711_33744[(2)] = inst_33676);

(statearr_33711_33744[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (2))){
var state_33690__$1 = state_33690;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33690__$1,(4),in$);
} else {
if((state_val_33691 === (23))){
var inst_33684 = (state_33690[(2)]);
var state_33690__$1 = state_33690;
var statearr_33712_33745 = state_33690__$1;
(statearr_33712_33745[(2)] = inst_33684);

(statearr_33712_33745[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (19))){
var inst_33671 = (state_33690[(2)]);
var state_33690__$1 = state_33690;
var statearr_33713_33746 = state_33690__$1;
(statearr_33713_33746[(2)] = inst_33671);

(statearr_33713_33746[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (11))){
var inst_33656 = (state_33690[(7)]);
var inst_33642 = (state_33690[(12)]);
var inst_33656__$1 = cljs.core.seq.call(null,inst_33642);
var state_33690__$1 = (function (){var statearr_33714 = state_33690;
(statearr_33714[(7)] = inst_33656__$1);

return statearr_33714;
})();
if(inst_33656__$1){
var statearr_33715_33747 = state_33690__$1;
(statearr_33715_33747[(1)] = (14));

} else {
var statearr_33716_33748 = state_33690__$1;
(statearr_33716_33748[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (9))){
var inst_33678 = (state_33690[(2)]);
var inst_33679 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_33690__$1 = (function (){var statearr_33717 = state_33690;
(statearr_33717[(15)] = inst_33678);

return statearr_33717;
})();
if(cljs.core.truth_(inst_33679)){
var statearr_33718_33749 = state_33690__$1;
(statearr_33718_33749[(1)] = (21));

} else {
var statearr_33719_33750 = state_33690__$1;
(statearr_33719_33750[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (5))){
var inst_33634 = cljs.core.async.close_BANG_.call(null,out);
var state_33690__$1 = state_33690;
var statearr_33720_33751 = state_33690__$1;
(statearr_33720_33751[(2)] = inst_33634);

(statearr_33720_33751[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (14))){
var inst_33656 = (state_33690[(7)]);
var inst_33658 = cljs.core.chunked_seq_QMARK_.call(null,inst_33656);
var state_33690__$1 = state_33690;
if(inst_33658){
var statearr_33721_33752 = state_33690__$1;
(statearr_33721_33752[(1)] = (17));

} else {
var statearr_33722_33753 = state_33690__$1;
(statearr_33722_33753[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (16))){
var inst_33674 = (state_33690[(2)]);
var state_33690__$1 = state_33690;
var statearr_33723_33754 = state_33690__$1;
(statearr_33723_33754[(2)] = inst_33674);

(statearr_33723_33754[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33691 === (10))){
var inst_33643 = (state_33690[(9)]);
var inst_33645 = (state_33690[(10)]);
var inst_33650 = cljs.core._nth.call(null,inst_33643,inst_33645);
var state_33690__$1 = state_33690;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33690__$1,(13),out,inst_33650);
} else {
if((state_val_33691 === (18))){
var inst_33656 = (state_33690[(7)]);
var inst_33665 = cljs.core.first.call(null,inst_33656);
var state_33690__$1 = state_33690;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33690__$1,(20),out,inst_33665);
} else {
if((state_val_33691 === (8))){
var inst_33645 = (state_33690[(10)]);
var inst_33644 = (state_33690[(11)]);
var inst_33647 = (inst_33645 < inst_33644);
var inst_33648 = inst_33647;
var state_33690__$1 = state_33690;
if(cljs.core.truth_(inst_33648)){
var statearr_33724_33755 = state_33690__$1;
(statearr_33724_33755[(1)] = (10));

} else {
var statearr_33725_33756 = state_33690__$1;
(statearr_33725_33756[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto__))
;
return ((function (switch__7585__auto__,c__7600__auto__){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33729 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33729[(0)] = state_machine__7586__auto__);

(statearr_33729[(1)] = (1));

return statearr_33729;
});
var state_machine__7586__auto____1 = (function (state_33690){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33690);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33730){if((e33730 instanceof Object)){
var ex__7589__auto__ = e33730;
var statearr_33731_33757 = state_33690;
(statearr_33731_33757[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33690);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33730;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33758 = state_33690;
state_33690 = G__33758;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33690){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33690);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto__))
})();
var state__7602__auto__ = (function (){var statearr_33732 = f__7601__auto__.call(null);
(statearr_33732[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto__);

return statearr_33732;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto__))
);

return c__7600__auto__;
});
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.mapcat_LT_ = (function() {
var mapcat_LT_ = null;
var mapcat_LT___2 = (function (f,in$){
return mapcat_LT_.call(null,f,in$,null);
});
var mapcat_LT___3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});
mapcat_LT_ = function(f,in$,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_LT___2.call(this,f,in$);
case 3:
return mapcat_LT___3.call(this,f,in$,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = mapcat_LT___2;
mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = mapcat_LT___3;
return mapcat_LT_;
})()
;
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.mapcat_GT_ = (function() {
var mapcat_GT_ = null;
var mapcat_GT___2 = (function (f,out){
return mapcat_GT_.call(null,f,out,null);
});
var mapcat_GT___3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});
mapcat_GT_ = function(f,out,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_GT___2.call(this,f,out);
case 3:
return mapcat_GT___3.call(this,f,out,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = mapcat_GT___2;
mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = mapcat_GT___3;
return mapcat_GT_;
})()
;
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.unique = (function() {
var unique = null;
var unique__1 = (function (ch){
return unique.call(null,ch,null);
});
var unique__2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__7600__auto___33855 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___33855,out){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___33855,out){
return (function (state_33830){
var state_val_33831 = (state_33830[(1)]);
if((state_val_33831 === (7))){
var inst_33825 = (state_33830[(2)]);
var state_33830__$1 = state_33830;
var statearr_33832_33856 = state_33830__$1;
(statearr_33832_33856[(2)] = inst_33825);

(statearr_33832_33856[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (1))){
var inst_33807 = null;
var state_33830__$1 = (function (){var statearr_33833 = state_33830;
(statearr_33833[(7)] = inst_33807);

return statearr_33833;
})();
var statearr_33834_33857 = state_33830__$1;
(statearr_33834_33857[(2)] = null);

(statearr_33834_33857[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (4))){
var inst_33810 = (state_33830[(8)]);
var inst_33810__$1 = (state_33830[(2)]);
var inst_33811 = (inst_33810__$1 == null);
var inst_33812 = cljs.core.not.call(null,inst_33811);
var state_33830__$1 = (function (){var statearr_33835 = state_33830;
(statearr_33835[(8)] = inst_33810__$1);

return statearr_33835;
})();
if(inst_33812){
var statearr_33836_33858 = state_33830__$1;
(statearr_33836_33858[(1)] = (5));

} else {
var statearr_33837_33859 = state_33830__$1;
(statearr_33837_33859[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (6))){
var state_33830__$1 = state_33830;
var statearr_33838_33860 = state_33830__$1;
(statearr_33838_33860[(2)] = null);

(statearr_33838_33860[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (3))){
var inst_33827 = (state_33830[(2)]);
var inst_33828 = cljs.core.async.close_BANG_.call(null,out);
var state_33830__$1 = (function (){var statearr_33839 = state_33830;
(statearr_33839[(9)] = inst_33827);

return statearr_33839;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33830__$1,inst_33828);
} else {
if((state_val_33831 === (2))){
var state_33830__$1 = state_33830;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33830__$1,(4),ch);
} else {
if((state_val_33831 === (11))){
var inst_33810 = (state_33830[(8)]);
var inst_33819 = (state_33830[(2)]);
var inst_33807 = inst_33810;
var state_33830__$1 = (function (){var statearr_33840 = state_33830;
(statearr_33840[(7)] = inst_33807);

(statearr_33840[(10)] = inst_33819);

return statearr_33840;
})();
var statearr_33841_33861 = state_33830__$1;
(statearr_33841_33861[(2)] = null);

(statearr_33841_33861[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (9))){
var inst_33810 = (state_33830[(8)]);
var state_33830__$1 = state_33830;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33830__$1,(11),out,inst_33810);
} else {
if((state_val_33831 === (5))){
var inst_33807 = (state_33830[(7)]);
var inst_33810 = (state_33830[(8)]);
var inst_33814 = cljs.core._EQ_.call(null,inst_33810,inst_33807);
var state_33830__$1 = state_33830;
if(inst_33814){
var statearr_33843_33862 = state_33830__$1;
(statearr_33843_33862[(1)] = (8));

} else {
var statearr_33844_33863 = state_33830__$1;
(statearr_33844_33863[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (10))){
var inst_33822 = (state_33830[(2)]);
var state_33830__$1 = state_33830;
var statearr_33845_33864 = state_33830__$1;
(statearr_33845_33864[(2)] = inst_33822);

(statearr_33845_33864[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33831 === (8))){
var inst_33807 = (state_33830[(7)]);
var tmp33842 = inst_33807;
var inst_33807__$1 = tmp33842;
var state_33830__$1 = (function (){var statearr_33846 = state_33830;
(statearr_33846[(7)] = inst_33807__$1);

return statearr_33846;
})();
var statearr_33847_33865 = state_33830__$1;
(statearr_33847_33865[(2)] = null);

(statearr_33847_33865[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___33855,out))
;
return ((function (switch__7585__auto__,c__7600__auto___33855,out){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33851 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33851[(0)] = state_machine__7586__auto__);

(statearr_33851[(1)] = (1));

return statearr_33851;
});
var state_machine__7586__auto____1 = (function (state_33830){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33830);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33852){if((e33852 instanceof Object)){
var ex__7589__auto__ = e33852;
var statearr_33853_33866 = state_33830;
(statearr_33853_33866[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33830);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33852;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33867 = state_33830;
state_33830 = G__33867;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33830){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33830);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___33855,out))
})();
var state__7602__auto__ = (function (){var statearr_33854 = f__7601__auto__.call(null);
(statearr_33854[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___33855);

return statearr_33854;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___33855,out))
);


return out;
});
unique = function(ch,buf_or_n){
switch(arguments.length){
case 1:
return unique__1.call(this,ch);
case 2:
return unique__2.call(this,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unique.cljs$core$IFn$_invoke$arity$1 = unique__1;
unique.cljs$core$IFn$_invoke$arity$2 = unique__2;
return unique;
})()
;
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.partition = (function() {
var partition = null;
var partition__2 = (function (n,ch){
return partition.call(null,n,ch,null);
});
var partition__3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__7600__auto___34002 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___34002,out){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___34002,out){
return (function (state_33972){
var state_val_33973 = (state_33972[(1)]);
if((state_val_33973 === (7))){
var inst_33968 = (state_33972[(2)]);
var state_33972__$1 = state_33972;
var statearr_33974_34003 = state_33972__$1;
(statearr_33974_34003[(2)] = inst_33968);

(statearr_33974_34003[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (1))){
var inst_33935 = (new Array(n));
var inst_33936 = inst_33935;
var inst_33937 = (0);
var state_33972__$1 = (function (){var statearr_33975 = state_33972;
(statearr_33975[(7)] = inst_33937);

(statearr_33975[(8)] = inst_33936);

return statearr_33975;
})();
var statearr_33976_34004 = state_33972__$1;
(statearr_33976_34004[(2)] = null);

(statearr_33976_34004[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (4))){
var inst_33940 = (state_33972[(9)]);
var inst_33940__$1 = (state_33972[(2)]);
var inst_33941 = (inst_33940__$1 == null);
var inst_33942 = cljs.core.not.call(null,inst_33941);
var state_33972__$1 = (function (){var statearr_33977 = state_33972;
(statearr_33977[(9)] = inst_33940__$1);

return statearr_33977;
})();
if(inst_33942){
var statearr_33978_34005 = state_33972__$1;
(statearr_33978_34005[(1)] = (5));

} else {
var statearr_33979_34006 = state_33972__$1;
(statearr_33979_34006[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (15))){
var inst_33962 = (state_33972[(2)]);
var state_33972__$1 = state_33972;
var statearr_33980_34007 = state_33972__$1;
(statearr_33980_34007[(2)] = inst_33962);

(statearr_33980_34007[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (13))){
var state_33972__$1 = state_33972;
var statearr_33981_34008 = state_33972__$1;
(statearr_33981_34008[(2)] = null);

(statearr_33981_34008[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (6))){
var inst_33937 = (state_33972[(7)]);
var inst_33958 = (inst_33937 > (0));
var state_33972__$1 = state_33972;
if(cljs.core.truth_(inst_33958)){
var statearr_33982_34009 = state_33972__$1;
(statearr_33982_34009[(1)] = (12));

} else {
var statearr_33983_34010 = state_33972__$1;
(statearr_33983_34010[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (3))){
var inst_33970 = (state_33972[(2)]);
var state_33972__$1 = state_33972;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33972__$1,inst_33970);
} else {
if((state_val_33973 === (12))){
var inst_33936 = (state_33972[(8)]);
var inst_33960 = cljs.core.vec.call(null,inst_33936);
var state_33972__$1 = state_33972;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33972__$1,(15),out,inst_33960);
} else {
if((state_val_33973 === (2))){
var state_33972__$1 = state_33972;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33972__$1,(4),ch);
} else {
if((state_val_33973 === (11))){
var inst_33952 = (state_33972[(2)]);
var inst_33953 = (new Array(n));
var inst_33936 = inst_33953;
var inst_33937 = (0);
var state_33972__$1 = (function (){var statearr_33984 = state_33972;
(statearr_33984[(10)] = inst_33952);

(statearr_33984[(7)] = inst_33937);

(statearr_33984[(8)] = inst_33936);

return statearr_33984;
})();
var statearr_33985_34011 = state_33972__$1;
(statearr_33985_34011[(2)] = null);

(statearr_33985_34011[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (9))){
var inst_33936 = (state_33972[(8)]);
var inst_33950 = cljs.core.vec.call(null,inst_33936);
var state_33972__$1 = state_33972;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33972__$1,(11),out,inst_33950);
} else {
if((state_val_33973 === (5))){
var inst_33937 = (state_33972[(7)]);
var inst_33936 = (state_33972[(8)]);
var inst_33940 = (state_33972[(9)]);
var inst_33945 = (state_33972[(11)]);
var inst_33944 = (inst_33936[inst_33937] = inst_33940);
var inst_33945__$1 = (inst_33937 + (1));
var inst_33946 = (inst_33945__$1 < n);
var state_33972__$1 = (function (){var statearr_33986 = state_33972;
(statearr_33986[(12)] = inst_33944);

(statearr_33986[(11)] = inst_33945__$1);

return statearr_33986;
})();
if(cljs.core.truth_(inst_33946)){
var statearr_33987_34012 = state_33972__$1;
(statearr_33987_34012[(1)] = (8));

} else {
var statearr_33988_34013 = state_33972__$1;
(statearr_33988_34013[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (14))){
var inst_33965 = (state_33972[(2)]);
var inst_33966 = cljs.core.async.close_BANG_.call(null,out);
var state_33972__$1 = (function (){var statearr_33990 = state_33972;
(statearr_33990[(13)] = inst_33965);

return statearr_33990;
})();
var statearr_33991_34014 = state_33972__$1;
(statearr_33991_34014[(2)] = inst_33966);

(statearr_33991_34014[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (10))){
var inst_33956 = (state_33972[(2)]);
var state_33972__$1 = state_33972;
var statearr_33992_34015 = state_33972__$1;
(statearr_33992_34015[(2)] = inst_33956);

(statearr_33992_34015[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33973 === (8))){
var inst_33936 = (state_33972[(8)]);
var inst_33945 = (state_33972[(11)]);
var tmp33989 = inst_33936;
var inst_33936__$1 = tmp33989;
var inst_33937 = inst_33945;
var state_33972__$1 = (function (){var statearr_33993 = state_33972;
(statearr_33993[(7)] = inst_33937);

(statearr_33993[(8)] = inst_33936__$1);

return statearr_33993;
})();
var statearr_33994_34016 = state_33972__$1;
(statearr_33994_34016[(2)] = null);

(statearr_33994_34016[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___34002,out))
;
return ((function (switch__7585__auto__,c__7600__auto___34002,out){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_33998 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33998[(0)] = state_machine__7586__auto__);

(statearr_33998[(1)] = (1));

return statearr_33998;
});
var state_machine__7586__auto____1 = (function (state_33972){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_33972);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e33999){if((e33999 instanceof Object)){
var ex__7589__auto__ = e33999;
var statearr_34000_34017 = state_33972;
(statearr_34000_34017[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33972);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33999;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34018 = state_33972;
state_33972 = G__34018;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_33972){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_33972);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___34002,out))
})();
var state__7602__auto__ = (function (){var statearr_34001 = f__7601__auto__.call(null);
(statearr_34001[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___34002);

return statearr_34001;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___34002,out))
);


return out;
});
partition = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition__2.call(this,n,ch);
case 3:
return partition__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition.cljs$core$IFn$_invoke$arity$2 = partition__2;
partition.cljs$core$IFn$_invoke$arity$3 = partition__3;
return partition;
})()
;
/**
* Deprecated - this function will be removed. Use transducer instead
*/
cljs.core.async.partition_by = (function() {
var partition_by = null;
var partition_by__2 = (function (f,ch){
return partition_by.call(null,f,ch,null);
});
var partition_by__3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__7600__auto___34161 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__7600__auto___34161,out){
return (function (){
var f__7601__auto__ = (function (){var switch__7585__auto__ = ((function (c__7600__auto___34161,out){
return (function (state_34131){
var state_val_34132 = (state_34131[(1)]);
if((state_val_34132 === (7))){
var inst_34127 = (state_34131[(2)]);
var state_34131__$1 = state_34131;
var statearr_34133_34162 = state_34131__$1;
(statearr_34133_34162[(2)] = inst_34127);

(statearr_34133_34162[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (1))){
var inst_34090 = [];
var inst_34091 = inst_34090;
var inst_34092 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_34131__$1 = (function (){var statearr_34134 = state_34131;
(statearr_34134[(7)] = inst_34091);

(statearr_34134[(8)] = inst_34092);

return statearr_34134;
})();
var statearr_34135_34163 = state_34131__$1;
(statearr_34135_34163[(2)] = null);

(statearr_34135_34163[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (4))){
var inst_34095 = (state_34131[(9)]);
var inst_34095__$1 = (state_34131[(2)]);
var inst_34096 = (inst_34095__$1 == null);
var inst_34097 = cljs.core.not.call(null,inst_34096);
var state_34131__$1 = (function (){var statearr_34136 = state_34131;
(statearr_34136[(9)] = inst_34095__$1);

return statearr_34136;
})();
if(inst_34097){
var statearr_34137_34164 = state_34131__$1;
(statearr_34137_34164[(1)] = (5));

} else {
var statearr_34138_34165 = state_34131__$1;
(statearr_34138_34165[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (15))){
var inst_34121 = (state_34131[(2)]);
var state_34131__$1 = state_34131;
var statearr_34139_34166 = state_34131__$1;
(statearr_34139_34166[(2)] = inst_34121);

(statearr_34139_34166[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (13))){
var state_34131__$1 = state_34131;
var statearr_34140_34167 = state_34131__$1;
(statearr_34140_34167[(2)] = null);

(statearr_34140_34167[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (6))){
var inst_34091 = (state_34131[(7)]);
var inst_34116 = inst_34091.length;
var inst_34117 = (inst_34116 > (0));
var state_34131__$1 = state_34131;
if(cljs.core.truth_(inst_34117)){
var statearr_34141_34168 = state_34131__$1;
(statearr_34141_34168[(1)] = (12));

} else {
var statearr_34142_34169 = state_34131__$1;
(statearr_34142_34169[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (3))){
var inst_34129 = (state_34131[(2)]);
var state_34131__$1 = state_34131;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34131__$1,inst_34129);
} else {
if((state_val_34132 === (12))){
var inst_34091 = (state_34131[(7)]);
var inst_34119 = cljs.core.vec.call(null,inst_34091);
var state_34131__$1 = state_34131;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_34131__$1,(15),out,inst_34119);
} else {
if((state_val_34132 === (2))){
var state_34131__$1 = state_34131;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34131__$1,(4),ch);
} else {
if((state_val_34132 === (11))){
var inst_34095 = (state_34131[(9)]);
var inst_34099 = (state_34131[(10)]);
var inst_34109 = (state_34131[(2)]);
var inst_34110 = [];
var inst_34111 = inst_34110.push(inst_34095);
var inst_34091 = inst_34110;
var inst_34092 = inst_34099;
var state_34131__$1 = (function (){var statearr_34143 = state_34131;
(statearr_34143[(7)] = inst_34091);

(statearr_34143[(11)] = inst_34111);

(statearr_34143[(12)] = inst_34109);

(statearr_34143[(8)] = inst_34092);

return statearr_34143;
})();
var statearr_34144_34170 = state_34131__$1;
(statearr_34144_34170[(2)] = null);

(statearr_34144_34170[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (9))){
var inst_34091 = (state_34131[(7)]);
var inst_34107 = cljs.core.vec.call(null,inst_34091);
var state_34131__$1 = state_34131;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_34131__$1,(11),out,inst_34107);
} else {
if((state_val_34132 === (5))){
var inst_34095 = (state_34131[(9)]);
var inst_34092 = (state_34131[(8)]);
var inst_34099 = (state_34131[(10)]);
var inst_34099__$1 = f.call(null,inst_34095);
var inst_34100 = cljs.core._EQ_.call(null,inst_34099__$1,inst_34092);
var inst_34101 = cljs.core.keyword_identical_QMARK_.call(null,inst_34092,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_34102 = (inst_34100) || (inst_34101);
var state_34131__$1 = (function (){var statearr_34145 = state_34131;
(statearr_34145[(10)] = inst_34099__$1);

return statearr_34145;
})();
if(cljs.core.truth_(inst_34102)){
var statearr_34146_34171 = state_34131__$1;
(statearr_34146_34171[(1)] = (8));

} else {
var statearr_34147_34172 = state_34131__$1;
(statearr_34147_34172[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (14))){
var inst_34124 = (state_34131[(2)]);
var inst_34125 = cljs.core.async.close_BANG_.call(null,out);
var state_34131__$1 = (function (){var statearr_34149 = state_34131;
(statearr_34149[(13)] = inst_34124);

return statearr_34149;
})();
var statearr_34150_34173 = state_34131__$1;
(statearr_34150_34173[(2)] = inst_34125);

(statearr_34150_34173[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (10))){
var inst_34114 = (state_34131[(2)]);
var state_34131__$1 = state_34131;
var statearr_34151_34174 = state_34131__$1;
(statearr_34151_34174[(2)] = inst_34114);

(statearr_34151_34174[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34132 === (8))){
var inst_34091 = (state_34131[(7)]);
var inst_34095 = (state_34131[(9)]);
var inst_34099 = (state_34131[(10)]);
var inst_34104 = inst_34091.push(inst_34095);
var tmp34148 = inst_34091;
var inst_34091__$1 = tmp34148;
var inst_34092 = inst_34099;
var state_34131__$1 = (function (){var statearr_34152 = state_34131;
(statearr_34152[(7)] = inst_34091__$1);

(statearr_34152[(14)] = inst_34104);

(statearr_34152[(8)] = inst_34092);

return statearr_34152;
})();
var statearr_34153_34175 = state_34131__$1;
(statearr_34153_34175[(2)] = null);

(statearr_34153_34175[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7600__auto___34161,out))
;
return ((function (switch__7585__auto__,c__7600__auto___34161,out){
return (function() {
var state_machine__7586__auto__ = null;
var state_machine__7586__auto____0 = (function (){
var statearr_34157 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34157[(0)] = state_machine__7586__auto__);

(statearr_34157[(1)] = (1));

return statearr_34157;
});
var state_machine__7586__auto____1 = (function (state_34131){
while(true){
var ret_value__7587__auto__ = (function (){try{while(true){
var result__7588__auto__ = switch__7585__auto__.call(null,state_34131);
if(cljs.core.keyword_identical_QMARK_.call(null,result__7588__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__7588__auto__;
}
break;
}
}catch (e34158){if((e34158 instanceof Object)){
var ex__7589__auto__ = e34158;
var statearr_34159_34176 = state_34131;
(statearr_34159_34176[(5)] = ex__7589__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34131);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34158;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7587__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34177 = state_34131;
state_34131 = G__34177;
continue;
} else {
return ret_value__7587__auto__;
}
break;
}
});
state_machine__7586__auto__ = function(state_34131){
switch(arguments.length){
case 0:
return state_machine__7586__auto____0.call(this);
case 1:
return state_machine__7586__auto____1.call(this,state_34131);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7586__auto____0;
state_machine__7586__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7586__auto____1;
return state_machine__7586__auto__;
})()
;})(switch__7585__auto__,c__7600__auto___34161,out))
})();
var state__7602__auto__ = (function (){var statearr_34160 = f__7601__auto__.call(null);
(statearr_34160[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7600__auto___34161);

return statearr_34160;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7602__auto__);
});})(c__7600__auto___34161,out))
);


return out;
});
partition_by = function(f,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition_by__2.call(this,f,ch);
case 3:
return partition_by__3.call(this,f,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition_by.cljs$core$IFn$_invoke$arity$2 = partition_by__2;
partition_by.cljs$core$IFn$_invoke$arity$3 = partition_by__3;
return partition_by;
})()
;
